<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class depositos extends Model
{
     /**
  * El nombre de la tabla donde se almacena los datos
  * @var String
  * @access protected
  */
  protected $table = 'depositos';

  /**
  * El nombre de la llave primaria
  * @var String
  * @access protected
  */
  protected $primaryKey = 'llave';

  /**
   * Los atributos que pueden ingresarlos de forma masiva
   *
   * @var array
   */
  protected $fillable = [
      'status',
      'cve_usuario',
      'numero_deposito',
      'monto_deposito',
      'banco_deposito',
      'deposito_fecha',
      'deposito_hora',
      'deposito_username',
      'deposito_clave',
      'fecha_sistema',
      'validacion',
      'num_movimiento',
      'num_sucursal',
      'ficha',
      'factura',
      'formas_pago',
      'cuenta_origen',
      'cfdi',
      'created_at',
      'updated_at',
      'tipo'
  ];
}
