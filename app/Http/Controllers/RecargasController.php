<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon ;


class RecargasController extends Controller
{
   
   public function __construct()
	{
	    $this->middleware('auth');
	}
   public function index(){
   	$role_name = (Auth::check()) ? Auth::user()->get() : false;
    $cve_usuario = Auth::user()->cve_usuario;
	  $saldo = $this->saldo($cve_usuario);
	 	$companias = DB::table('ta_companias AS tc')
        ->select('tc.id AS idcompania','tc.nombre','tc.img','tc.orden','tc.activo','tc.codigo','tc.imagens AS imagen')
        ->where('tc.activow','=',"1")
        ->get();
   

   	return view('recargas.addRecarga', compact('companias','saldo'));

   }
   public function montoSearch(Request $request){
   		
   		
   		$montos = DB::table('ta_companias_montos AS tcm')
        ->select('tcm.id','tcm.compania','tcm.monto','tam.nombre','tam.monto','tam.codigo')
        ->join('ta_montos AS tam', 'tam.id', '=', 'tcm.monto')
        ->where('tcm.activo',1)
        ->where('tcm.compania',$request->id)
        ->orderBy('tam.monto', 'ASC')
        ->get();

        return $montos;
   }

   public function viewSearch(Request $request){

		$viewmontos = DB::table('ta_companias_montos  AS tcm')
    ->select('tam.monto', 'tcm.descripcion', 'tac.nombre' )
		->join('ta_montos  AS tam', 'tam.id', '=', 'tcm.monto')
		->join('ta_companias AS tac', 'tac.id', '=', 'tcm.compania')
		->where('tcm.id',$request->montoview)->first();

		$datos = array(
			'nombre' => $viewmontos->nombre,
			'monto' => $viewmontos->monto,
      'descripcion' => $viewmontos->descripcion
		);

		return $datos;
		
   }
   public function recargaAdd(Request $request){
   	 try{

   	 	if(!isset($_SERVER['HTTP_REFERER'])){
		      return redirect()->back()->with('message', 'No puedes acceder directamente.');
		}


   	 	$rules = [
          'pania' => 'required|max:1',
          'otnom' => 'required|numeric',
          'enohp' => 'required|numeric',
          'textNip' => 'required',
        ];

        $rulesMessage = [
          'pania.required' => '* Compañia es obligatorio.',
          'otnom.required' => '* El Monto es obligatorio.',
          'enohp.required' => '* El campo teléfono es obligatorio, debe contener 10 numeros.',
          'enohp.numeric' => '* El campo teléfono debe contener numeros.',
          'textNip.required' => '* NIP es obligatorio.',          
        ];

        $validator = Validator::make($request->all(),  $rules , $rulesMessage);

        if ($validator->fails()) {
          return redirect()->back()->withInput()->with([
             'field_errors' => $validator->errors()
           ]);
        }

	   	$userId = Auth::id();
      $monto_compania = $this->search_m_c($request->otnom);
      $nip = Auth::user()->nip;
      $cve_usuario = Auth::user()->cve_usuario;
      $saldo_user = $this->saldo($cve_usuario);

               if($nip == $request->textNip){

                  if($saldo_user->saldo>=$monto_compania->monto)
                  {
                     $monto_r = encrypt($monto_compania->monto);
                     $montol = encrypt($monto_compania->nombre);
                     $compania_r = encrypt($monto_compania->img);
                     $telefono_r = encrypt($request->enohp);
                     //dd('envio recarga');
                     return redirect('/send-recarga/'.$compania_r.'/'.$telefono_r.'/'.$monto_r.'/'.$montol);
                  }
                  else{
                    //dd('error');
                   return redirect()->back()->with('message', 'Necesitas aumentar tu saldo, para hacer una recarga.'); 
                    
                  }
                } else{
                   //dd('error de pin');
                   return redirect()->back()->with('message', 'NIP de Seguridad Incorrecto.');
                }

	  }
	  catch(\Exception $e){
        Auth::logout();
        $e;
        //return view('Verror', ['message' => $e->getMessage().', ALProveedoresCompletarAuth.']);
      } 	

   }
   public function requestRecarga($xml, $phone, $monto, $compania, $montol){
    $xml = decrypt($xml);
    
    $resultado = preg_replace("/<.xml(.*?)>/", '', $xml);
    $resultado_xml = substr($resultado, 1);
    $list_xml = simplexml_load_string($resultado_xml);
    
    $fecha = $list_xml->fecha;
    $status = $list_xml->status;
    $folio = $list_xml->folio;
    $descripcion = $list_xml->descripcion;


       $dat = array(
          "phone" => $phone,
          "monto" => $monto,
          "compania" => $compania,
          "montol" => $montol
        );
        
    $data = encrypt($dat);

    return view('recargas.viewConfirmaRecarga', compact('fecha','status','folio', 'descripcion', 'phone', 'data'));
   }

   public function printRecarga($data, $folio, $fecha){
    $data = decrypt($data);

    //dd($data);
    //echo 'alis';
    $cliente = Auth::user()->cliente;
    $rfc = Auth::user()->rfc;
    $calle_num = Auth::user()->calle_num;
    $colonia = Auth::user()->colonia;
    $del_mun = Auth::user()->del_mun;
    $cp = Auth::user()->cp;
    $telefono1 = Auth::user()->telefono1;
    $phone = $data['phone'];
    $compania = $data['compania'];
    $monto = $data['monto'];
    $montol = ucwords($data['montol']);


    return view('recargas.impresion', compact('cliente','rfc','calle_num','colonia','del_mun','cp','telefono1','folio','fecha','phone','compania','monto','montol'));

   }

   public function searchUser(){

   		$userId = Auth::id();

   		$user = DB::table('usuarios')
        ->where('id','=',$userId)
        ->get();

        return $user;

   }
   public function saldo($cve_usuario){

      $user = DB::table('saldos')
        ->where('cve_usuario','=',$cve_usuario)
        ->first();

        return $user;
   }

   public function search_m_c($monto){

      $viewmontos = DB::table('ta_companias_montos AS tcm')
      ->select('tcm.id','tcm.compania','tam.nombre','tam.monto','tam.codigo', 'tac.nombre AS nombrec', 'tac.img')
    ->join('ta_montos AS tam', 'tam.id', '=', 'tcm.monto')
    ->join('ta_companias AS tac', 'tac.id', '=', 'tcm.compania')
    ->where('tcm.id',$monto)->first();

        return $viewmontos;
   }

   public function addRecargar(Request $request){
   	 try{

   	 	if(!isset($_SERVER['HTTP_REFERER'])){
		      return redirect()->back()->with('message', 'No puedes acceder directamente.');
		}


   	 	$rules = [
          'textNip' => 'required',
          'data' => 'required'
        ];

        $rulesMessage = [
          'textNip.required' => '* El NIP es obligatorio.',
          'data.required' => '* Hay un Problema en tu Recarga Intenta de Nuevo.'
        ];

        $validator = Validator::make($request->all(),  $rules , $rulesMessage);

        if ($validator->fails()) {
          return redirect()->back()->withInput()->with([
             'field_errors' => $validator->errors()
           ]);
        }
        $data  = $request->data;
        $lectura = $request->lectura;
        $lectura  = decrypt($lectura);
        $readonlys =  $lectura;
        $readonlyss = encrypt($readonlys);
        $datad = decrypt($data); 
        $monto_compania = $this->search_m_c($datad['monto']);
        $nip = Auth::user()->nip;
        $cve_usuario = Auth::user()->cve_usuario;
        $saldo_user = $this->saldo($cve_usuario);
                 
          //dd($monto_compania);
        		    if($nip == $request->textNip){

                  if($saldo_user->saldo>=$monto_compania->monto)
                  {
                     $monto_r = encrypt($monto_compania->monto);
                     $monto_l = encrypt($monto_compania->nombre);
                     $compania_r = encrypt($monto_compania->img);
                     $telefono_r = encrypt($datad['phone']);
                     return redirect('/send-recarga/'.$compania_r.'/'.$telefono_r.'/'.$monto_r.'/'.$monto_l);
                  }
                  else{
                    $message = 2;
                   return view('recargas.nipConfirma', compact('data', 'message', 'readonlys','readonlyss'));
                  }
                } else{
                   $message = 1;
                   return view('recargas.nipConfirma', compact('data', 'message', 'readonlys','readonlyss'));
                }
	  }
	  catch(\Exception $e){
        Auth::logout();
        $e;
        echo $e->getMessage();
        //return view('Verror', ['message' => $e->getMessage().', ALProveedoresCompletarAuth.']);
      } 	

   }

    

}
