<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon ;


class UsuariosController extends Controller
{
   
   public function __construct()
	{
	    $this->middleware('auth');
	}
   public function perfil($tipo=0){
   	$cliente = Auth::user();
   	return view('usuario.perfilUsuario', compact('cliente','tipo'));

   }

   public function contrasena(){
    $cliente = Auth::user();
    return view('usuario.contrasena', compact('cliente'));

   }

   public function correctLenght($rfc)
    {
        $length = mb_strlen($rfc);
        // El RFC debe ser de 12 letras para personas morales y 13 para personas
        // físicas, cualquier longitud fuera de ese rango resulta en un RFC
        // inválido.
        return $length >= 12 && $length <= 13;
    }

   public function valida_rfc($valor){
         $valor = str_replace("-", "", $valor); 
         $cuartoValor = substr($valor, 3, 1);
         //RFC sin homoclave
         if(strlen($valor)==10){
             $letras = substr($valor, 0, 4); 
             $numeros = substr($valor, 4, 6);
             if (ctype_alpha($letras) && ctype_digit($numeros)) { 
                 return true;
             }
             return false;            
         }
         // Sólo la homoclave
         else if (strlen($valor) == 3) {
             $homoclave = $valor;
             if(ctype_alnum($homoclave)){
                 return true;
             }
             return false;
         }
         //RFC Persona Moral.
         else if (ctype_digit($cuartoValor) && strlen($valor) == 12) { 
             $letras = substr($valor, 0, 3); 
             $numeros = substr($valor, 3, 6); 
             $homoclave = substr($valor, 9, 3); 
             if (ctype_alpha($letras) && ctype_digit($numeros) && ctype_alnum($homoclave)) { 
                 return true; 
             } 
             return false;
         //RFC Persona Física. 
         } else if (ctype_alpha($cuartoValor) && strlen($valor) == 13) { 
             $letras = substr($valor, 0, 4); 
             $numeros = substr($valor, 4, 6);
             $homoclave = substr($valor, 10, 3); 
             if (ctype_alpha($letras) && ctype_digit($numeros) && ctype_alnum($homoclave)) { 
                 return true; 
             }
             return false; 
         }else { 
             return false; 
         }  
  } 

   public function updateUser(Request $request){

    try{

        if(!isset($_SERVER['HTTP_REFERER'])){
          return redirect()->back()->with('message', 'No puedes acceder directamente.');
        }

        $rules = [
          'cliente' => 'required',
          'email' => 'required',
          'nombres' => 'required',
          'apellido1' => 'required',
          'apellido2' => 'required',
          'calle_num' => 'required',
          'colonia' => 'required',
          'del_mun' => 'required',
          'ciudad' => 'required',
          'cp' => 'required'
        ];

        $rulesMessage = [
          'cliente.required' => '* Razon Social es obligatorio.',
          'email.required' => '* Correo Electronico es obligatorio.',
          'nombres.required' => '* Nombre(s) de Representante es obligatorio.',
          'apellido1.required' => '* El Apellido Paterno de Representante es obligatorio.',
          'apellido2.required' => '* El Apellido Materno de Representante es obligatorio.',
          'calle_num.required' => '* Calle y Numero es obligatorio.',
          'colonia.required' => '* Colonia es obligatorio.',
          'del_mun.required' => '* Delegacion o Municipio es obligatorio.',
          'ciudad.required' => '* Ciudad es obligatorio.',
          'cp.required' => '* Codigo Postal es obligatorio.'

          
        ];

        $validator = Validator::make($request->all(),  $rules , $rulesMessage);

        if ($validator->fails()) {
          return redirect()->back()->withInput()->with([
             'field_errors' => $validator->errors()
           ]);
        }

        if($request->tipop > 0){

          $rfc =  $this->valida_rfc($request->rfc);

          if($rfc == true){
              $rfca = strtoupper($request->rfc);
          }else{
              return redirect()->back()->with('error', 'RFC Incorrecto.');
          }

        }  
        
        $array = array(
          "cliente" => strtoupper($request->cliente),
          "rfc" => strtoupper($request->rfc),
          "email" => $request->email,
          "nombres" => strtoupper($request->nombres),
          "apellido1" => strtoupper($request->apellido1),
          "apellido2" => strtoupper($request->apellido2),
          "calle_num" => $request->calle_num,
          "colonia" => strtoupper($request->colonia),
          "del_mun" => strtoupper($request->del_mun),
          "ciudad" => strtoupper($request->ciudad),
          "cp" => $request->cp,
          "telefono2" => $request->telefono2,
          "tipo_persona" =>$request->tipop
        );

        $id = Auth::id();
        $actualiza = DB::table('usuarios')
        ->where('id', $id)
        ->update($array);

        if($request->tipo == "1")
        {
              
              return redirect('/depositos-add/1');  
              
        }else{
              return redirect()->back()->with('message', 'Perfil Actualizado!');
            }
              
        
        
    }
    catch(\Exception $e){
        Auth::logout();
        $e;
        //return view('Verror', ['message' => $e->getMessage().', Mensagge.']);
      }     

   }

   public function updateContrasena(Request $request){

    try{

        if(!isset($_SERVER['HTTP_REFERER'])){
          return redirect()->back()->with('message', 'No puedes acceder directamente.');
        }

        $rules = [
          'pass1' => 'required',
          'pass2' => 'required'
        ];

        $rulesMessage = [
          'pass1.required' => '* Contrasena es obligatorio.',
          'pass2.required' => '* Confirmar Contraseña es obligatorio.'
        ];

        $validator = Validator::make($request->all(),  $rules , $rulesMessage);

        if ($validator->fails()) {
          return redirect()->back()->withInput()->with([
             'field_errors' => $validator->errors()
           ]);
        }
        
        $array = array(
          "password" => md5($request->pass1)
        );

        $id = Auth::id();
        $actualiza = DB::table('usuarios')
        ->where('id', $id)
        ->update($array);
        
        
        return redirect('login')->with(Auth::logout());
        //return redirect()->back()->with('message', 'Contraseña Actualizada!');
        
    }
    catch(\Exception $e){
        Auth::logout();
        $e;
        //return view('Verror', ['message' => $e->getMessage().', ALProveedoresCompletarAuth.']);
      }     

   }
  
   public function utilerias(){
    $cliente = Auth::user();
    return view('usuario.utilerias');

   }

   


}
