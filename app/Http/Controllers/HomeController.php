<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\User;
use App\models\mensajes_sms_salida;
use App\lib\nusoap\nusoap;
use Carbon\Carbon ;
use Illuminate\Support\Facades\Validator;
class HomeController extends Controller
{

    public function index(Request $request)
    {

        $user = Auth::ID();

        if (is_null($user)) {
          return redirect('/login');
        }

        $role = DB::table('role_user')
        ->where('user_id','=',$user)
        ->first();

        $roles = $role->role_id;

        if ($roles != 2){
            Auth::logout();
            return redirect('/permiso-denegado');
        }

        $request->user()->authorizeRoles(['user']);
        $saldo = $this->saldo()->saldo;
        $recargas = $this->recargasToday();

        //dd($saldo);
        return view('home', compact('saldo', 'recargas'));
    }  

      public function saldo(){
        $cve_usuario = Auth::user()->cve_usuario;
        $user = DB::table('saldos')
          ->where('cve_usuario','=',$cve_usuario)
          ->first();

          return $user;
     }


      public function recargasToday(){
        $cve_usuario = Auth::user()->cve_usuario;

        $user = DB::table('estados_cuenta')
        ->whereDate('fecha', Carbon::today())
        ->where('cargo', '!=' ,'0.00')
        ->where('cve_usuario','=',$cve_usuario)
        ->get();

        $sum = 0;
        foreach ($user as $v) {
          $sum +=  $v->cargo;
        }

          return $sum;
     }

      public function resetear(){
        return view('auth.reset');
      }
      public function login2(){
        return view('auth.login2');
      }

      public function peticionReload(){
        return view('auth.peticionReload');
      }

      public function peticionView($llave, $pass){
        try {

          $token = DB::table('mensajes_sms_salida')
          ->where([
                    ['llave', '=', $llave],
                    ['status', '=', 1],
                ])
          ->first();

          $token_d = encrypt($token->llave);


          $date_created = new \Carbon\Carbon($token->created_at);
          $date_created = $date_created->format('Y-m-d');


           if($date_created == date("Y-m-d"))
           {
              return view('auth.peticion',compact('token_d', 'pass'));
           }else{
             return view('auth.peticionReload');
           }
            
         }
          catch(\Exception $e) {
              return $e->getMessage();
          }   
      }
      public function token_send(Request $request){
        try {

          $token = DB::table('mensajes_sms_salida')
          ->where([
                    ['llave', '=', $llave],
                    ['status', '=', 1],
                ])
          ->first();

          $date_created = new \Carbon\Carbon($token->created_at);
          $date_created = $date_created->format('Y-m-d');


           if($date_created == date("Y-m-d"))
           {
              return view('auth.peticion');
           }else{
             return view('auth.peticionReload');
           }
            
         }
          catch(\Exception $e) {
              return $e->getMessage();
          }   
      }

      public function peticion(Request $request){
          try {
                $cve_usuario = $request->input('telefono');
                $password    = $request->input('password');
                $password  = md5($password);

                $users = DB::table('usuarios')->where([
                    ['cve_usuario', '=', $cve_usuario],
                    ['password', '=', $password],
                ])->first();


                $pass = encrypt($password);

                $token_view = DB::table('mensajes_sms_salida')
                ->where('cve_usuario', '=', $cve_usuario)
                ->whereDate('created_at', '=', Carbon::now()->format('Y-m-d'))
                ->first();
                $token_c = count($token_view);

                $users_c = count($users);

                if ($users_c == 0){
                  return redirect()->back()->with('message', 'Número o contraseña incorrectos, verifica e ingresa de nuevo tus datos. ');
                } else {
                  if($token_c == 1)
                  {
                    return redirect('/sendmsg/'.$token_view->token.'/'.$users->cve_usuario.'/'.$token_view->llave.'/'.$pass);
                    //return redirect('/peticion-view/'.$token_view->llave.'/'.$pass);
                  }else{
                                    mt_srand(time());
                                    $digitos = '';
                                    for($i = 0; $i < 4; $i++){
                                       $digitos .= mt_rand(0,9);
                                    }
                                    
                                    $mensaje ='Tu Token es: '.$digitos.'. Vigente para el dia de hoy.';
                  
                                    $sms = new mensajes_sms_salida();
                                    $sms->fecha_sistema = date("Y-m-d H:i:s");
                                    $sms->cve_usuario = $users->cve_usuario;
                                    $sms->mensaje =  $mensaje;
                                    $sms->status = 1;
                                    $sms->numero_destino = $users->cve_usuario;
                                    $sms->token = $digitos;
                                    $sms->save();
                                   
                  
                                    return redirect('/sendmsg/'.$digitos.'/'.$users->cve_usuario.'/'.$sms->llave.'/'.$pass);
                                  }
                }


              
          }
          catch(\Exception $e) {
              return $e->getMessage();
          }

          
        }

        public function resetearBuscar(Request $request){
          $user = $request->input('email');
          $proveedor = DB::table('users')
          ->where('users.email',$user)
          ->first();

          if (is_null($proveedor)){
            return redirect()->back()->with('message', 'Dirección de correo no encontrada. ');
          } else {

            $email = $proveedor->email;
            $name = $proveedor->name;
            $id = $proveedor->id;
            $this->welcomeEmailRec($email,$name,$id);
            return redirect('/envio-recuperar-contrasena');
          }
        }
        public function accesoA(Request $request){
          try {
                $token      = $request->input('token');
                $token_d    = $request->input('token_d');
                $token_d    = decrypt($token_d);

                $users = DB::table('mensajes_sms_salida')->where([
                    ['llave', '=', $token_d],
                    ['token', '=', $token],
                ])->first();
                
                $users_c = count($users);

                if ($users_c == 0){
                  return redirect()->back()->with('message', 'Token incorrecto, verifica e ingresa de nuevo el token. ');
                } else {
                  echo 'entro al home';
                }


              
          }
          catch(\Exception $e) {
              return $e->getMessage();
          }
        }

        public function envio(){
          return view('auth.revisa');
        }

        public function Vpermisodenegado(){
            return view('Vpermisodenegado');
        }

        public function smsError(){
            return view('VerrorSMS');
        }

        public function peticionReset(){
         return view('auth.peticionReload');
        }

        public function resertContrasena(Request $request){

                $users = DB::table('usuarios')->where('cve_usuario', '=', $request->phone)->first();
                
                mt_srand(time());
                $digitos = '';
                for($i = 0; $i < 4; $i++){
                    $digitos .= mt_rand(0,9);
                }

                $mensaje ='Tu Token para cambio de contraseña es: '.$digitos.'.';
                  
                                    $sms = new mensajes_sms_salida();
                                    $sms->fecha_sistema = date("Y-m-d H:i:s");
                                    $sms->cve_usuario = $users->cve_usuario;
                                    $sms->mensaje =  $mensaje;
                                    $sms->status = 1;
                                    $sms->numero_destino = $users->cve_usuario;
                                    $sms->token = $digitos;
                                    $sms->save();


                if (count($users) == 0){
                  return redirect()->back()->with('message', 'El número de teléfono que ingreso, no existe. ');
                } else {

                  return redirect('/sendmsg-restart/'.$digitos.'/'.$users->cve_usuario);
                }

          
        }

        public function updateContrasenafree(Request $request){
          try {

            $cve_usuario = decrypt($request->token_d);
            $cve_usuario1 =$request->token_d;
            $tokens = decrypt($request->token_t);
            
            $token = DB::table('mensajes_sms_salida')
            ->where([
                      ['cve_usuario', '=', $cve_usuario],
                      ['token', '=', $tokens],
                      ['status', '=', 1],
                  ])
            ->first();


            $date_created = new \Carbon\Carbon($token->created_at);
            $date_created = $date_created->format('Y-m-d');


             if($date_created == date("Y-m-d"))
             {
                return view('auth.contrasena', compact('cve_usuario1'));
             }else{
               return view('auth.peticionReload');
             }
              
           }
            catch(\Exception $e) {
                return $e->getMessage();
            }   
          //dd($request);
        }

        public function updateContrasena(Request $request){

    try{

        if(!isset($_SERVER['HTTP_REFERER'])){
          return redirect()->back()->with('message', 'No puedes acceder directamente.');
        }

        $rules = [
          'pass1' => 'required',
          'pass2' => 'required'
        ];

        $rulesMessage = [
          'pass1.required' => '* Contrasena es obligatorio.',
          'pass2.required' => '* Confirmar Contraseña es obligatorio.'
        ];

        $validator = Validator::make($request->all(),  $rules , $rulesMessage);

        if ($validator->fails()) {
          return redirect()->back()->withInput()->with([
             'field_errors' => $validator->errors()
           ]);
        }
        
        $array = array(
          "password" => md5($request->pass1)
        );

        $cve_usuario = decrypt($request->tokend);

        $id = Auth::id();
        $actualiza = DB::table('usuarios')
        ->where('cve_usuario', $cve_usuario)
        ->update($array);
        

        return redirect('/login')->with('status', 'Profile updated!');
        
        
    }
    catch(\Exception $e){
        echo $e;
        //return view('Verror', ['message' => $e->getMessage().', ALProveedoresCompletarAuth.']);
      }     

   }

/*
    public function someAdminStuff(Request $request)
    {
        $request->user()->authorizeRoles(‘admin’);
        return view(‘some.view’);
    }
    */
}
