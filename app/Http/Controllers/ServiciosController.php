<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon ;


class ServiciosController extends Controller
{
   
   public function __construct()
	{
	    $this->middleware('auth');
	}
   public function index(){
   	$role_name = (Auth::check()) ? Auth::user()->get() : false;
	
	 	$companias = DB::table('ta_pago_servicio')
        ->where('activo','=',"1")
        ->get();


   	return view('servicios.addServicio', compact('companias'));

   }
   public function montoSearch(Request $request){
   		
   		
   		$montos = DB::table('ta_companias_montos')
        ->join('ta_montos', 'ta_montos.idmontos', '=', 'ta_companias_montos.monto')
        ->where('ta_companias_montos.activo',1)
        ->where('ta_companias_montos.compania',$request->id)
        ->get();

        return $montos;
   }

   public function viewSearch(Request $request){

		$viewmontos = DB::table('ta_pago_servicio')
		->where('id',$request->servicio)->first();

		$datos = array(
      'idservice' => $viewmontos->id,
			'nombre' => $viewmontos->nombre,
			'imagen' => $viewmontos->imagen
		);

		return $datos;
		
   }
   public function recargaAdd(Request $request){
   	 try{

   	 	if(!isset($_SERVER['HTTP_REFERER'])){
		      return redirect()->back()->with('message', 'No puedes acceder directamente.');
		}


   	 	$rules = [
          'compania' => 'required|max:1',
          'mento' => 'required|numeric',
          'phone' => 'required|numeric',
        ];

        $rulesMessage = [
          'compania.required' => '* Compañia es obligatorio.',
          'mento.required' => '* El Monto es obligatorio.',
          'phone.required' => '* El campo teléfono es obligatorio, debe contener 10 numeros.',
          'phone.numeric' => '* El campo teléfono debe contener numeros.',
          
        ];

        $validator = Validator::make($request->all(),  $rules , $rulesMessage);

        if ($validator->fails()) {
          return redirect()->back()->withInput()->with([
             'field_errors' => $validator->errors()
           ]);
        }

	   	$userId = Auth::id();
	   	$data =array(
	   		'phone' => $request->phone,
	   		'compania' => $request->compania,
	   		'monto' => $request->mento
	   	);
	   	
	   	$data = encrypt($data);
      $message = 0;
      $readonlys = array(
        'monto' => $request->textMonto,
        'compania' => $request->textCompania,
        'telefono' => $request->phone
      );
      $readonlyss =encrypt($readonlys);
	   	return view('recargas.nipConfirma', compact('data','message','readonlys', 'readonlyss'));
	  }
	  catch(\Exception $e){
        Auth::logout();
        $e;
        //return view('Verror', ['message' => $e->getMessage().', ALProveedoresCompletarAuth.']);
      } 	

   }
   public function searchUser(){

   		$userId = Auth::id();

   		$user = DB::table('usuarios')
        ->where('id','=',$userId)
        ->get();

        return $user;

   }
   public function saldo($cve_usuario){

      $user = DB::table('saldos')
        ->where('cve_usuario','=',$cve_usuario)
        ->first();

        return $user;
   }

   public function search_m_c($monto){

      $viewmontos = DB::table('ta_companias_montos')
    ->join('ta_montos', 'ta_montos.idmontos', '=', 'ta_companias_montos.monto')
    ->join('ta_companias', 'ta_companias.idcompania', '=', 'ta_companias_montos.compania')
    ->where('ta_companias_montos.id',$monto)->first();

        return $viewmontos;
   }

   public function addRecargar(Request $request){
   	 try{

   	 	if(!isset($_SERVER['HTTP_REFERER'])){
		      return redirect()->back()->with('message', 'No puedes acceder directamente.');
		}


   	 	$rules = [
          'textNip' => 'required',
          'data' => 'required'
        ];

        $rulesMessage = [
          'textNip.required' => '* El NIP es obligatorio.',
          'data.required' => '* Hay un Problema en tu Recarga Intenta de Nuevo.'
        ];

        $validator = Validator::make($request->all(),  $rules , $rulesMessage);

        if ($validator->fails()) {
          return redirect()->back()->withInput()->with([
             'field_errors' => $validator->errors()
           ]);
        }
        $data  = $request->data;
        $lectura = $request->lectura;
        $lectura  = decrypt($lectura);
        $readonlys =  $lectura;
        $readonlyss = encrypt($readonlys);
        $datad = decrypt($data); 
        $monto_compania = $this->search_m_c($datad['monto']);
        $nip = Auth::user()->nip;
        $cve_usuario = Auth::user()->cve_usuario;
        $saldo_user = $this->saldo($cve_usuario);
                 
          //dd($saldo);
        		    if($nip == $request->textNip){

                  if($saldo_user->saldo>=$monto_compania->monto)
                  {
                     echo "hacemos recarga";
                  }
                  else{
                    echo 'no tienes alcance';
                  }
                  dd($saldo);
                } else{
                   $message = 1;
                   return view('recargas.nipConfirma', compact('data', 'message', 'readonlys','readonlyss'));
                }
	  }
	  catch(\Exception $e){
        Auth::logout();
        $e;
        echo $e->getMessage();
        //return view('Verror', ['message' => $e->getMessage().', ALProveedoresCompletarAuth.']);
      } 	

   }

    

}
