<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\models\depositos;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;


class DepositosController extends Controller
{
   
   public function __construct()
	{
	    $this->middleware('auth');
	}
   
   public function index(){
    $role_name = (Auth::check()) ? Auth::user()->get() : false;
  
    

    return view('depositos.Deposito');

   }

   public function index2($factura=0){
   	$role_name = (Auth::check()) ? Auth::user()->get() : false;
	
	 	$bancos = DB::table('bancos')->get();
    $formas_pago = DB::table('formas_pago')->get();
    $cp = $cliente = Auth::user()->cp;

   	return view('depositos.addDepositos', compact('bancos','formas_pago', 'factura', 'cp'));

   }
  
   public function reportes(){
    $role_name = (Auth::check()) ? Auth::user()->get() : false;
    $cve_usuario = Auth::user()->cve_usuario;
  
    $depositos = DB::table('estados_cuenta')
    ->where('cve_usuario',$cve_usuario)
    ->get();

    

    return view('depositos.reportesDepositos', compact('depositos'));

   }
   public function viewdeposito($numero_deposito){
    $cve_usuario = Auth::user()->cve_usuario;
    $mes = date('m');

    $depositos = DB::table('depositos')
    ->where('numero_deposito',$numero_deposito)
    ->where('cve_usuario',$cve_usuario)
    ->whereMonth('created_at', $mes)
    ->first(); 

    return $depositos;   

   }

   public function depositosAdd(Request $request){
   	 try{

   	 	if(!isset($_SERVER['HTTP_REFERER'])){
		      return redirect()->back()->with('message', 'No puedes acceder directamente.');
		}


   	 	$rules = [
          'banco_deposito' => 'required',
          'monto_deposito' => 'required',
          'deposito_fecha' => 'required',
          'deposito_hora' => 'required',
          'numero_sucursal' => 'required',
          'numero_deposito' => 'required',
          'file' => 'required'
        ];

        $rulesMessage = [
          'banco_deposito.required' => '* El Banco es obligatorio.',
          'monto_deposito.required' => '* El Monto es obligatorio.',
          'deposito_fecha.required' => '* La Fecha de Depósito es obligatorio.',
          'deposito_hora.required' => '* La Hora de Depósito es obligatorio.',
          'numero_sucursal.required' => '* El Número de Sucursal es obligatorio.',
          'numero_deposito.required' => '* El Número de Operación es obligatorio.',
          'file.required' => '* Ficha de Deposito es obligatorio.'
          
        ];

        $validator = Validator::make($request->all(),  $rules , $rulesMessage);

        if ($validator->fails()) {
          return redirect()->back()->withInput()->with([
             'field_errors' => $validator->errors()
           ]);
        }

	   	   $userId = Auth::id();
         $cve_usuario = Auth::user()->cve_usuario;
         
          $ficha = $request->file->store('fichasd');
          $cuenta_origen = $request->cuenta_origen ? $request->cuenta_origen : '';
          $cfdi = $request->cfdi ? $request->cfdi : '';
          $factura = $request->factura ? $request->factura : '';

          $previewDeposito = array(
            "cve_usuario" => $cve_usuario,
            "banco_deposito" => $request->banco_deposito,
            "formas_pago" => $request->formas_pago,
            "cuenta_origen" => $cuenta_origen,
            "cfdi" => $cfdi,
            "monto_deposito" => $request->monto_deposito,
            "deposito_fecha" => $request->deposito_fecha,
            "deposito_hora" => $request->deposito_hora,
            "numero_sucursal" => $request->numero_sucursal,
            "numero_deposito" => $request->numero_deposito,
            "num_movimiento" => $request->num_movimiento,
            "ficha" => $ficha,
            'factura' => $factura
          );

            $depositamos = encrypt($previewDeposito);
            $formas_pago = DB::table('formas_pago')->where('id_formas_pago',$request->formas_pago)->first();
            $formas_pago_desc = $formas_pago->nombre;

            $verdeposito = $this->viewdeposito($request->numero_deposito);

          if($request->deposito_fecha <=  date("Y-m-d"))
          {
            if(count($verdeposito) != 0)
            {
              return redirect()->back()->with('message', 'El Depósito que deseas ingresar, ya esta en la base de datos.');         
            }else{
                    return view('depositos.previewDeposito', compact('previewDeposito','formas_pago_desc', 'depositamos'));
                  }  
          }
          else{
            return redirect()->back()->with('message', 'No puedes ingresar depositos Futuros.');
          }  

          
	  }
	  catch(\Exception $e){
        Auth::logout();
        $e;
        //return view('Verror', ['message' => $e->getMessage().', ALProveedoresCompletarAuth.']);
      } 	

   }

   function clean($string) {
       $string = str_replace(' ', '', $string); // Replaces all spaces with hyphens.

       return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
   }


   public function searchDeposito(){
      $role_name = (Auth::check()) ? Auth::user()->get() : false;
      $cve_usuario = Auth::user()->cve_usuario;

      $depositos = DB::table('estados_cuenta')
      ->where('cve_usuario',$cve_usuario)
      ->get();


      return view('depositos.reportesEstadocuenta', compact('depositos'));

   }
   public function depositamosAdd(Request $request){
    try{

      if(!isset($_SERVER['HTTP_REFERER'])){
            return redirect()->back()->with('message', 'No puedes acceder directamente.');
      }

          $depositamos   = decrypt($request->depositamos);
          
          $cuenta_origen = $depositamos['cuenta_origen'] ? $depositamos['cuenta_origen'] : 'null';
          $cfdi = $depositamos['cfdi'] ? $depositamos['cfdi'] : '0';
          $factura = $depositamos['factura'] ? $depositamos['factura'] : '0'; 
          //dd($depositamos);
          $cve_usuario = $depositamos['cve_usuario'];
          $numero_deposito = $depositamos['numero_deposito'];
          $monto_deposito = $depositamos['monto_deposito'];
          $monto_deposito = $this->clean($monto_deposito);
          $banco_deposito = $depositamos['banco_deposito'];
          $deposito_fecha = $depositamos['deposito_fecha'];
          $deposito_hora = $depositamos['deposito_hora'];
          $num_movimiento = $depositamos['num_movimiento'];
          $numero_sucursal = $depositamos['numero_sucursal'];
          $ficha = $depositamos['ficha'];
          $formas_pago = $depositamos['formas_pago'];
          

          $depositos = new depositos();
          $depositos->status = '0';
          $depositos->cve_usuario = $cve_usuario;
          $depositos->numero_deposito = $numero_deposito;
          $depositos->monto_deposito = $monto_deposito;
          $depositos->banco_deposito = $banco_deposito;
          $depositos->deposito_fecha = $deposito_fecha;
          $depositos->deposito_hora = $deposito_hora;
          $depositos->num_movimiento = $num_movimiento;
          $depositos->num_sucursal = $numero_sucursal;
          $depositos->ficha = $ficha;
          $depositos->formas_pago =$formas_pago;
          $depositos->factura =$factura;
          $depositos->tipo ='1';
          $depositos->save();
          return view('VdepositoM');  
    }
    catch(\Exception $e){
        //Auth::logout();
        echo $e;
        //return view('Verror', ['message' => $e->getMessage().', ALProveedoresCompletarAuth.']);
      }  

   }

   public function searchUser(){

   		$userId = Auth::id();

   		$user = DB::table('usuarios')
        ->where('id','=',$userId)
        ->get();

        return $user;

   }
   public function saldo($cve_usuario){

      $user = DB::table('saldos')
        ->where('cve_usuario','=',$cve_usuario)
        ->first();

        return $user;
   }

   public function search_m_c($monto){

      $viewmontos = DB::table('ta_companias_montos')
    ->join('ta_montos', 'ta_montos.idmontos', '=', 'ta_companias_montos.monto')
    ->join('ta_companias', 'ta_companias.idcompania', '=', 'ta_companias_montos.compania')
    ->where('ta_companias_montos.id',$monto)->first();

        return $viewmontos;
   }

   public function reporteSearch(Request $request){

      $cve_usuario = Auth::user()->cve_usuario; 
      $mes = $request->mes;
      
      $depositos = DB::table('estados_cuenta')
      ->whereMonth('fecha', '=', $mes)
      ->where('cve_usuario',$cve_usuario)
      ->get();


      return $depositos;
    
   }


   public function addRecargar(Request $request){
   	 try{

   	 	if(!isset($_SERVER['HTTP_REFERER'])){
		      return redirect()->back()->with('message', 'No puedes acceder directamente.');
		}


   	 	$rules = [
          'textNip' => 'required',
          'data' => 'required'
        ];

        $rulesMessage = [
          'textNip.required' => '* El NIP es obligatorio.',
          'data.required' => '* Hay un Problema en tu Recarga Intenta de Nuevo.'
        ];

        $validator = Validator::make($request->all(),  $rules , $rulesMessage);

        if ($validator->fails()) {
          return redirect()->back()->withInput()->with([
             'field_errors' => $validator->errors()
           ]);
        }
        $data  = $request->data;
        $lectura = $request->lectura;
        $lectura  = decrypt($lectura);
        $readonlys =  $lectura;
        $readonlyss = encrypt($readonlys);
        $datad = decrypt($data); 
        $monto_compania = $this->search_m_c($datad['monto']);
        $nip = Auth::user()->nip;
        $cve_usuario = Auth::user()->cve_usuario;
        $saldo_user = $this->saldo($cve_usuario);
                 
          //dd($saldo);
        		    if($nip == $request->textNip){

                  if($saldo_user->saldo>=$monto_compania->monto)
                  {
                     echo "hacemos recarga";
                  }
                  else{
                    echo 'no tienes alcance';
                  }
                  dd($saldo);
                } else{
                   $message = 1;
                   return view('recargas.nipConfirma', compact('data', 'message', 'readonlys','readonlyss'));
                }
	  }
	  catch(\Exception $e){
        Auth::logout();
        $e;
        echo $e->getMessage();
        //return view('Verror', ['message' => $e->getMessage().', ALProveedoresCompletarAuth.']);
      } 	

   }

   public function reportesVentas(){
    $cve_usuario = Auth::user()->cve_usuario;
    $cargo = DB::table('estados_cuenta')
    ->where('cve_usuario',$cve_usuario)
    ->where('cargo', '!=' ,'0.00')
    ->get();

    return view('depositos.reportesVentas', compact('cargo'));

   }


   public function reporteSearchv(Request $request){

      $cve_usuario = Auth::user()->cve_usuario; 
      $mes = $request->mes;
      $ano = $request->year;
      $phone = $request->phone; 
      
      if($phone != '' && $mes != '0'){
        $depositos = DB::table('estados_cuenta')
        ->where('cve_usuario',$cve_usuario)
        ->where('cargo', '!=' ,'0.00')
        ->whereMonth('fecha', '=', $mes)
        ->where('referencia2', 'like', '%' .$phone. '%')
        ->get();

      }
      else if($phone != '' && $mes == '0'){
        $depositos = DB::table('estados_cuenta')
        ->where('cve_usuario',$cve_usuario)
        ->where('cargo', '!=' ,'0.00')
        ->where('referencia2', 'like', '%' .$phone. '%')
        ->get();
      }
      else{
        $depositos = DB::table('estados_cuenta')
        ->where('cve_usuario',$cve_usuario)
        ->whereMonth('fecha', '=', $mes)
        ->where('cargo', '!=' ,'0.00')
        ->get(); 
      }



      return $depositos;
    
   }
   public function reportesDepositos(){
      $cve_usuario = Auth::user()->cve_usuario;

      $depositos = DB::table('depositos AS d')
      ->select('d.llave', 'd.status', 'd.cve_usuario', 'd.numero_deposito', 'd.monto_deposito', 'd.banco_deposito', 'd.deposito_fecha', 'd.deposito_hora', 'd.deposito_username', 'd.deposito_clave', 'd.fecha_sistema', 'd.validacion', 'd.num_movimiento', 'd.num_sucursal', 'd.ficha', 'd.factura', 'd.formas_pago', 'd.cuenta_origen', 'd.cfdi', 'd.created_at', 'd.updated_at','d.comentarios', 'fm.id_formas_pago', 'fm.nombre')
      ->Join('formas_pago AS fm', 'fm.id_formas_pago', '=', 'd.formas_pago')
      ->where('d.cve_usuario',$cve_usuario)
      ->get();

      return view('depositos.reportesDepositos', compact('depositos'));

   }

   public function depositoSearch(Request $request){

      $cve_usuario = Auth::user()->cve_usuario; 
      $mes = $request->mes;

      $depositos = DB::table('depositos AS d')
      ->select('d.llave', 'd.status', 'd.cve_usuario', 'd.numero_deposito', 'd.monto_deposito', 'd.banco_deposito', 'd.deposito_fecha', 'd.deposito_hora', 'd.deposito_username', 'd.deposito_clave', 'd.fecha_sistema', 'd.validacion', 'd.num_movimiento', 'd.num_sucursal', 'd.ficha', 'd.factura', 'd.formas_pago', 'd.cuenta_origen', 'd.cfdi', 'd.created_at', 'd.updated_at','d.comentarios', 'fm.id_formas_pago', 'fm.nombre')
      ->Join('formas_pago AS fm', 'fm.id_formas_pago', '=', 'd.formas_pago')
      ->where('d.cve_usuario',$cve_usuario)
      ->whereMonth('d.deposito_fecha', '=', $mes)
      ->get();
    
      return $depositos;
    
   }


    

}
