<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();

// Route para el logout.
Route::get('/logout', 'Auth\LoginController@logout');
Route::post('/peticion', 'HomeController@peticion');
Route::get('/login2', 'HomeController@login2');
Route::post('/peticion-reload', 'HomeController@peticionReload');
Route::get('/peticion-view/{llave}/{pass}', 'HomeController@peticionView');
Route::get('/resetear-contrasena', 'HomeController@resetear');
Route::post('/reset-contrasena', 'HomeController@resetearBuscar');
Route::get('/envio-recuperar-contrasena', 'HomeController@envio');
Route::get('/home', [
      'as'    =>  'home',
      'uses'  =>  'HomeController@index'
   ]);

Route::get('/', function() {
      return redirect('/login');
});
Route::any('/sendmsg/{token}/{telefono}/{llave}/{pass}', function($token, $telefono, $llave, $pass) {
  
    include 'nusoap/nusoap.php';
    $wsdl = "http://sms.sispro.mx/ws/status/soap.php?wsdl";

    $client = new nusoap_client($wsdl, 'wsdl');
    $err = $client->getError();
    if ($err) {
       // Display the error
       echo '<h2>Constructor error</h2>' . $err;
       // At this point, you know the call that follows will fail
       exit();
    }

    $mensaje ='Tu Token es: '.$token .'. Vigente para el dia de hoy.';
    $parametros = array(
                'usuario'     => '5556938963',
                'password'    => '22222222',
                'dongle'      => 'popocaca',
                'telefono'     => $telefono,
                'mensaje'     => $mensaje
            );
    $result1=$client->call('sendMsg', $parametros);

    if($result1 == null)
    {
      return redirect('/SMError');
    }else{
      return redirect('/peticion-view/'.$llave.'/'.$pass);
    }
   

});

Route::get('/SMError', 'HomeController@smsError');

Route::post('/acceso', 'Auth\LoginController@acceso');

Route::get('/permiso-denegado', 'HomeController@Vpermisodenegado');

Route::get('/recargas', 'RecargasController@index');
Route::post('/recargas-add', 'RecargasController@recargaAdd');
Route::post('/recarga-add', 'RecargasController@recargarAdd');
Route::post('/add-recargar', 'RecargasController@addRecargar');
Route::post('/monto', 'RecargasController@montoSearch');
Route::post('/monto-view', 'RecargasController@viewSearch');

Route::any('/send-recarga/{compania}/{telefono}/{monto}/{montol}/', function($compania, $telefono, $monto, $montol) {
  
    include 'nusoap/nusoap.php';

    $wsdl = "http://webservice.tumundodeprepago.com/tmpagoventaws/status/soap.php?wsdl";

    $client = new nusoap_client($wsdl, 'wsdl');
    $err = $client->getError();
    if ($err) {
       // Display the error
       echo '<h2>Constructor error</h2>' . $err;
       // At this point, you know the call that follows will fail
       exit();
    }

    $compania_r = decrypt($compania);
    $telefono_r = decrypt($telefono);
    $monto_r = decrypt($monto);
    $montol = decrypt($montol);
    $consecutivo = Auth::user()->consecutivo + 1;
    $cve_usuario = Auth::user()->cve_usuario;
    $nip = Auth::user()->nip;
    $dongle = Auth::user()->dongle;

    $id_cliente = 'PRO'.date("mdY").date("Hms").$consecutivo;
    
    $parametros = array(
                'usuario'     => $cve_usuario,
                'password'    => $nip,
                'producto'    => '',
                'monto'       => $monto_r,
                'compania'    => $compania_r,
                'telefono'    => $telefono_r,
                'dongle'      => $dongle,
                'id_cliente'  => $id_cliente
            );

   
        //dd($parametros);
    
    $result1=$client->call('ventaTiempoAire', $parametros);
    $result = encrypt($result1);

   return redirect('/recarga-request/'.$result.'/'.$telefono_r.'/'.$monto_r.'/'.$compania_r.'/'.$montol);
   //return redirect('/recarga-request/'.$result.'/'.$telefono_r.'/'.$compania_r.'/'.$monto_r);
});

Route::get('/recarga-print/{phone}/{folio}/{fecha}', 'RecargasController@printRecarga');

Route::get('/recarga-request/{xml}/{phone}/{monto}/{compania}/{montol}', 'RecargasController@requestRecarga');

Route::get('/servicios', 'ServiciosController@index');

Route::post('/servicio-view', 'ServiciosController@viewSearch');

Route::get('/depositos', 'DepositosController@index');

Route::get('/depositos-add/{factura}', 'DepositosController@index2');

Route::post('/add-depositos', 'DepositosController@depositosAdd');

Route::post('/add-depositamos', 'DepositosController@depositamosAdd');

Route::get('/depositoMSN', 'DepositosController@msnDeposito');

Route::get('/depositos-rep', 'DepositosController@searchDeposito');

Route::post('/reporte-search', 'DepositosController@reporteSearch');

Route::post('/reporte-searchv', 'DepositosController@reporteSearchv');

Route::get('/reporte-ven', 'DepositosController@reportesVentas');

Route::get('/reporte-deposito', 'DepositosController@reportesDepositos');

Route::post('/deposito-search', 'DepositosController@depositoSearch');

Route::post('/servicio-view', 'ServiciosController@viewSearch');

Route::get('/perfil-usuario/{tipo}', 'UsuariosController@perfil');

Route::post('/update-user', 'UsuariosController@updateUser');

Route::get('/contrasena', 'UsuariosController@contrasena');

Route::post('/update-contrasena', 'UsuariosController@updateContrasena');

Route::get('/utilerias', 'UsuariosController@utilerias');

Route::post('/resert-contrasena', 'UsuariosController@updateContrasena');

Route::post('/peticion-restart', 'HomeController@resertContrasena');

Route::any('/sendmsg-restart/{token}/{telefono}', function($token, $telefono) {
  
    include 'nusoap/nusoap.php';
    $wsdl = "http://sms.sispro.mx/ws/status/soap.php?wsdl";

    $client = new nusoap_client($wsdl, 'wsdl');
    $err = $client->getError();
    if ($err) {
       // Display the error
       echo '<h2>Constructor error</h2>' . $err;
       // At this point, you know the call that follows will fail
       exit();
    }

    $mensaje ='Tu Token para cambio de contraseña es: '.$token .'.';
    $parametros = array(
                'usuario'     => '5556938963',
                'password'    => '22222222',
                'dongle'      => 'popocaca',
                'telefono'     => $telefono,
                'mensaje'     => $mensaje
            );
    $result1=$client->call('sendMsg', $parametros);

    $phone = encrypt($telefono);
    $token_1 = encrypt($token);

    if($result1 == null)
    {
      return redirect('/SMError');
    }else{
      
      return redirect('/token-restart/'.$phone.'/'.$token_1);

    }
   

});

Route::get('/token-restart/{telefono}/{token}', function($telefono, $token) {
   return view('auth.peticionR', compact('telefono', 'token'));
});

Route::post('/update-password-f', 'HomeController@updateContrasenafree');

Route::post('/update-contrasena-f', 'HomeController@updateContrasena');