@extends('layouts.logged')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">

                <div class="card-body">
                 <h1>Movimientos de Hoy </h1>
                </div>
            </div>
        </div>
    </div>

    <div class="container mt-12">
    <div class="row">
        <div class="col-md-4">
            <div class="progress-bar1" data-percent="{{$saldo}}" data-duration="1000" data-color="#ccc,yellow"></div>
            <div class="col-md-12 col-sm-6 text-center"><h3>Total de Saldo </h3></div>
        </div>
        <div class="col-md-4">
            <div class="progress-bar1" data-percent="{{$recargas}}" data-duration="1000" data-color="#ccc,red"></div>
            <div class="col-md-12 col-sm-6 text-center"><h3>Recargas de Hoy</h3></div>
        </div>
        <div class="col-md-4">
            <div class="progress-bar1" data-percent="0.1" data-duration="1000" data-color="#ccc,green"></div>
            <div class="col-md-12 col-sm-6 text-center"><h3>Pago de Servicios</h3></div>
        </div>
    </div>
    </div>

</div>

<style type="text/css">
    .progress-bar1 {
  position: relative;
  height: 200px;
  width: 200px;
  margin:0 auto;
}
 
.progress-bar1 div {
  position: absolute;
  height: 200px;
  width: 200px;
  border-radius: 50%;
}
 
.progress-bar1 div span {
  position: absolute;
  font-family: Arial;
  font-size: 40px;
  line-height: 150px;
  height: 150px;
  width: 150px;
  left: 25px;
  top: 25px;
  text-align: center;
  border-radius: 50%;
  background-color: white;
}
 
.progress-bar1 .background { background-color: #b3cef6; }
 
.progress-bar1 .rotate {
  clip: rect(0 50px 100px 0);
  background-color: #4b86db;
}
 
.progress-bar1 .left {
  clip: rect(0 100px 200px 0);
  opacity: 1;
  background-color: #b3cef6;
}
 
.progress-bar1 .right {
  clip: rect(0 100px 200px 0);
  transform: rotate(180deg);
  opacity: 0;
  background-color: #4b86db;
}
 
@keyframes
toggle {  0% {
 opacity: 0;
}
 100% {
 opacity: 1;
}
}   

</style>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.js"></script>
<script src="{{asset('js/jQuery-plugin-progressbar.js')}}"></script>
<script>
$(".progress-bar1").loading();
     
</script>
@endsection
