@extends('layouts.logged')
@section('content')

<div class="col-md-12 col-xs-12">
 <div class="x_panel">
                  <div class="x_title">
                    <h2>Agregar Deposito</h2>
                    <div class="clearfix"></div>
                  </div>
  <br/>
                
                  <div class="x_content">
                    <br>
                    <form class="form-horizontal form-label-left"  method="POST" enctype="multipart/form-data" autocomplete="off" action="{{ url('/add-depositos') }}">
                      {{ csrf_field() }}

                      @if(session()->has('field_errors'))
                            <div class="col-md-12 col-xs-12 w3-panel w3-red w3-display-container">
                              <span onclick="this.parentElement.style.display='none'" class="w3-button w3-red w3-large w3-display-topright">×</span>
                              <h3>Datos Requeridos!</h3>
                                @foreach (session()->get('field_errors')->all() as $error)
                                <div>{{ $error }}</div>
                              @endforeach
                            </div>
                      @endif
                      @if(session()->has('message'))
                          <div class="alert alert-danger">
                              {{ session()->get('message') }}
                          </div>
                      @endif
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Banco</label>
                        <div class="col-md-4 col-sm-9 col-xs-12">
                          <select class="form-control" required name="banco_deposito">
                            <option>Selecciona una opción</option>
                            @foreach ($bancos as $com)
                              <option value="{{$com->banco}}">{{$com->banco}}</option>
                            @endforeach
                          </select>
                        </div>
                      </div>
                       <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Formas de Pago</label>
                        <div class="col-md-4 col-sm-9 col-xs-12">
                          <select class="form-control" required name="formas_pago" id="formas_pago">
                            <option>Selecciona una opción</option>
                            @foreach ($formas_pago as $com)
                              <option value="{{$com->id_formas_pago}}">{{$com->nombre}}</option>
                            @endforeach
                          </select>
                        </div>
                      </div>
                      <div id="transfer" style="display: none;">
                        
                      </div>  

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Monto</label>
                        <div class="col-md-4 col-sm-9 col-xs-12">
                            <input type="text" name="monto_deposito" value="{{ old('monto_deposito') }}" id="monto" required class="form-control currency">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Fecha de Depósito</label>
                        <div class="col-md-4 col-sm-9 col-xs-12">
                          <input type="date" class="form-control" name="deposito_fecha" value="{{ old('deposito_fecha') }}" required />
                        <!--
                        <div class="input-group date" id="myDatepicker2">
                            <input type="text" readonly name="deposito_fecha" requerid class="form-control">
                            <span class="input-group-addon">
                               <span class="glyphicon glyphicon-calendar"></span>
                            </span>
                        </div>-->
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12"><i class="fa fa-clock-o"></i> Hora de Depósito</label>
                        <div class="col-md-4 col-sm-9 col-xs-12">
                            <input type="text" class="form-control timepicker" readonly name="deposito_hora" value="{{ old('deposito_hora') }}" required placeholder="04:32:05 PM" />
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Número de Sucursal</label>
                        <div class="col-md-4 col-sm-9 col-xs-12">
                            <input type="text" required name="numero_sucursal" value="{{ old('numero_sucursal') }}"  required class="form-control">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Número de Autorización</label>
                        <div class="col-md-4 col-sm-9 col-xs-12">
                            <input type="text" required name="numero_deposito" value="{{ old('numero_deposito') }}" class="form-control">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="exampleFormControlFile1" class="control-label col-md-3 col-sm-3 col-xs-12">Ficha de Deposito</label>
                        <div class="col-md-4 col-sm-9 col-xs-12">
                        <input type="file" name="file" class="form-control" required="" id="exampleFormControlFile1">
                      </div>
                      </div>
                      
                      </div>
                      


                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                          <input type="hidden" name="factura" value="{{$factura}}" >
                          <button type="submit" class="btn btn-success"><i class="fa fa-paper-plane" aria-hidden="true"></i> Enviar</button>
                        </div>
                      </div>
                      

                    </form>
                  </div>
                </div>
              </div>

  <style>
  .w3-red, .w3-hover-red:hover {
      color: #fff!important;
      background-color: #f44336!important;
  }
  .w3-panel {
      margin-top: 16px;
      margin-bottom: 16px;
  }
  .w3-container, .w3-panel {
      padding: 0.01em 16px;
  }
  .w3-tooltip, .w3-display-container {
      position: relative;
  }
  #timepicker {
  width: 20%;
  margin: 30px 40%;
  margin-bottom: 0;
  position: absolute;
}

.timepicker_wrap {
  top: 40px;
  display: none;
  position: absolute;
  background: hsl(0, 0%, 100%);
  border-radius: 5px;
  z-index: 125;
  width: 100%;
}

.hour, .min, .meridian, .sec {
  float: left;
  width: 60px;
  margin: 0 10px;
}

.timepicker_wrap .btn {
  background: url( '{{ url('images/arrow.png') }}' ) no-repeat;
  cursor: pointer;
  padding: 18px;
  border: 3px dotted hsl(120, 40%, 80%);
  width: 28%;
  margin: auto;
  border-radius: 5px;
}

.timepicker_wrap .prev { background-position: 50% -50%; }

.timepicker_wrap .next { background-position: 50% 150%; }

.timepicker_wrap .ti_tx {
  margin: 10px 0;
  width: 100%;
  text-align: center;
}

.timepicker_wrap .in_txt {
  border-radius: 10px;
  width: 70%;
  float: none;
  text-align: center;
  padding-bottom: 8px;
  border: 2px solid hsl(0, 0%, 0%);
  font-family: georgia, helvetica, arial;
  font-weight: 700;
  font-size: 1.5em;
}

.timepicker_wrap > .hour > .ti_tx:after {
  content: '';
  font-size: 1em;
  padding-left: 0.2em;
  font-weight: 700;
}

.timepicker_wrap >.min > .ti_tx:after {
  content: '';
  font-size: 1em;
  padding-left: 0.2em;
  font-weight: 700;
}

.timepicker_wrap > .sec > .ti_tx:after {
  content: '';
  font-size: 1em;
  padding-left: 0.2em;
  font-weight: 700;
}

.timepicker_wrap > .meridian > .ti_tx:after {
  content: '';
  font-size: 1em;
  padding-left: 0.2em;
  font-weight: 700;
}
  </style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="{{asset('js/pickerjs.js')}}"></script>
<script>
jQuery.fn.getNum = function() {
    var val = $.trim($(this).val());
    if(val.indexOf(',') > -1) {
        val = val.replace(',', '.');
    }
    var num = parseFloat(val);
    var num = num.toFixed(2);
    if(isNaN(num)) {
        num = '';
    }
    return num;
}

$(function() {

    $('#monto').blur(function() {
        $(this).val($(this).getNum());
    });

});

$('select#formas_pago').on('change',function(){
    var valor = $(this).val();
    if(valor == 3)
    {
      $("#transfer").show();
      $("#transfer").append('<div class="form-group"><label class="control-label col-md-3 col-sm-3 col-xs-12">Cuenta de Origen</label><div class="col-md-4 col-sm-9 col-xs-12"><input type="text" name="cuenta_origen" id="cuenta_origen" required class="form-control"></div></div><div class="form-group"><label class="control-label col-md-3 col-sm-3 col-xs-12">Codigo Postal</label><div class="col-md-4 col-sm-9 col-xs-12"><input type="number" readonly value="{{$cp}}" class="form-control"></div></div><div class="form-group"><label class="control-label col-md-3 col-sm-3 col-xs-12">Uso de CFDI</label><div class="col-md-4 col-sm-9 col-xs-12"><select class="form-control" required name="cfdi"><option>Selecciona una opción</option><option value="1">Gastos en General</option><option value="2">Adquisición de Mercancias</option></select></div></div>');
    }else{
      $("#transfer").empty();
    }
});


</script>
@endsection
