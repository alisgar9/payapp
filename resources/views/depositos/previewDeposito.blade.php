@extends('layouts.logged')
@section('content')
<div class="col-md-12 col-xs-12">
 <div class="x_panel">
                  <div class="x_title">
                    <h2>Confirmar Nuevo Deposito</h2>
                    <div class="clearfix"></div>
                  </div>
  <br/>
                  <div class="col-md-3"></div>
                  <div class="col-md-8" style="background:#e6e6e6">
                   
                    <div class="col-md-12">
                    <form class="form-horizontal form-label-left"  method="POST" action="{{ url('/add-depositamos') }}" style="font-size: 20px;">
                      {{ csrf_field() }}

                      @if(session()->has('field_errors'))
                            <div class="col-md-12 col-xs-12 w3-panel w3-red w3-display-container">
                              <span onclick="this.parentElement.style.display='none'" class="w3-button w3-red w3-large w3-display-topright">×</span>
                              <h3>Datos Requeridos!</h3>
                                @foreach (session()->get('field_errors')->all() as $error)
                                <div>{{ $error }}</div>
                              @endforeach
                            </div>
                      @endif
                      <p><br><h3><b>&nbsp;&nbsp; *Verifica que los datos sean correctos</b></h3></p>
                      <hr style="border: solid 1px #73879C; background:#73879C; ">
                      <div class="form-group">
                        <label class="control-label col-md-5">Banco:</label>
                        <div class="col-md-7">
                          <label class="control-label" style="font-weight: 400">
                            <?php echo $previewDeposito['banco_deposito']; ?>
                          </label>  
                        </div>
                      </div> 
                      <div class="form-group">
                        <label class="control-label col-md-5">Formas de Pago:</label>
                        <div class="col-md-7">
                          <label class="control-label" style="font-weight: 400">
                            <?php echo $formas_pago_desc; ?>
                          </label>  
                        </div>
                      </div>

                      @if($previewDeposito['formas_pago'] == 3)
                       
                      <div class="form-group">
                        <label class="control-label col-md-5">Cuenta de Origen:</label>
                        <div class="col-md-7">
                          <label class="control-label" style="font-weight: 400">
                            <?php echo $previewDeposito['cuenta_origen']; ?>
                          </label>  
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-5">Uso de CFDI:</label>
                        <div class="col-md-7">
                          <label class="control-label" style="font-weight: 400">
                            <?php if($previewDeposito['cfdi'] == 1){ echo 'Gastos en General'; }else{ echo 'Adquisición de Mercancias';} ?>
                          </label>  
                        </div>
                      </div>


                      @endif 

                      <div class="form-group">
                        <label class="control-label col-md-5">Monto:</label>
                        <div class="col-md-7">
                          <label class="control-label" style="font-weight: 400">
                            $<?php echo number_format((float)$previewDeposito['monto_deposito'], 2, '.', ''); ?>
                          </label>  
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-5">Fecha de Depósito:</label>
                        <div class="col-md-7">
                          <label class="control-label" style="font-weight: 400">
                            <?php echo $previewDeposito['deposito_fecha']; ?>
                          </label>  
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-5"><i class="fa fa-clock-o"></i> Hora de Depósito</label>
                        <div class="col-md-7">
                          <label class="control-label" style="font-weight: 400">
                            <?php echo $previewDeposito['deposito_hora']; ?>
                          </label>  
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-5">Número de Sucursal</label>
                        <div class="col-md-7">
                          <label class="control-label" style="font-weight: 400">
                            <?php echo $previewDeposito['numero_sucursal']; ?>
                          </label>  
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-5">Número de Autorización</label>
                        <div class="col-md-7">
                            <label class="control-label" style="font-weight: 400">
                              <?php echo $previewDeposito['numero_deposito']; ?>
                            </label>  
                        </div>
                      </div>

                        <div class="form-group">
                          <div class="col-md-4 text-center">
                            <input type="hidden" name="depositamos" value="{{$depositamos}}">
                            <button type="submit" class="btn btn-success"><i class="fa fa-paper-plane" aria-hidden="true"></i> Los Datos son Correctos</button>
                          </div>
                          <div class="col-md-4 text-center">
                            <a href="{{ URL::previous() }}" class="btn btn-success"> <i class="fa fa-pencil" aria-hidden="true"></i> Corregir</a>
                          </div>
                          <div class="col-md-4 text-center">
                            <a href="{{ url('/home') }}" class="btn btn-success"><i class="fa fa-times" aria-hidden="true"></i> Cancelar</a>
                          </div>
                        </div>
                      </div>
                      

                      <hr style="border: solid 1px #73879C; background:#73879C; ">
                      
                      </div>
                      <br/>

                    </form>

                  </div>

                  </div>
                </div>
              </div>

  <style>
  .w3-red, .w3-hover-red:hover {
      color: #fff!important;
      background-color: #f44336!important;
  }
  .w3-panel {
      margin-top: 16px;
      margin-bottom: 16px;
  }
  .w3-container, .w3-panel {
      padding: 0.01em 16px;
  }
  .w3-tooltip, .w3-display-container {
      position: relative;
  }
  </style>

@endsection
