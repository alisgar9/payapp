@extends('layouts.logged')
@section('content')

<div class="col-md-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Estado de Cuenta</h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <br>
                    <p>Generar Reporte por Mes.</p>

                    <form id="form" method="post" class="form-vertical form-label-left">
                      {{ csrf_field() }}
                      <div class="form-group">
                        <div class="col-md-4">
                          <select name="mes" requerid class="form-control">
                              <option value="0">Selecciona una Opción</option>
                              <option value="1">Enero</option>
                              <option value="2">Febrero</option>
                              <option value="3">Marzo</option>
                              <option value="4">Abril</option>
                              <option value="5">Mayo</option>
                              <option value="6">Junio</option>
                              <option value="7">Julio</option>
                              <option value="8">Agosto</option>
                              <option value="9">Septiembre</option>
                              <option value="10">Octubre</option>
                              <option value="11">Noviembre</option>
                              <option value="12">Diciembre</option>
                          </select>
                        </div>

                        <div class="col-md-4">
                          <button type="button" id="btn_enviar" class="btn btn-success">Generar</button>
                        </div>
                      </div>
                    </form>
                    
            <div class="table-responsive">
            <table id="datatable2" class="table table-striped table-bordered dataTable no-footer" role="grid" aria-describedby="datatable_info">
                  <thead>
                     <tr class="headings" align="center">
                        <th class="column-title" width="20%" style="display: table-cell;">Referencia</th>
                        <th class="column-title" width="20%" style="display: table-cell;">Cargo </th>
                        <th class="column-title" width="10%" style="display: table-cell;">Abono</th>
                        <th class="column-title" width="10%" style="display: table-cell;">Saldo </th>
                        <th class="column-title" width="10%" style="display: table-cell;">fecha</th>
                        <th class="column-title" width="10%" style="display: table-cell;">Descripción</th>
                     </tr>
                  </thead>
            </table>
            <div class="col-md-12 text-center" id="resultado"></div>
            <table id="datatable" class="table table-striped table-bordered dataTable no-footer" role="grid" aria-describedby="datatable_info">
                  <tbody>
                     @foreach ($depositos as $bm)
                     <tr class="even pointer">
                        <td class="text-center" width="20%" >{{$bm->referencia}}</td>
                        <td class="text-center" width="20%" >${{$bm->cargo}}</td>
                        <td class="text-center" width="10%" >${{$bm->abono}}</td>
                        <td class="text-center" width="10%" >${{$bm->saldo}}</td>
                        <td class="text-center" width="10%" >{{$bm->fecha}}</td>
                        <td class="text-center" width="10%" >{{$bm->referencia2}}</td>
                     </tr>
                     @endforeach
                      
                  </tbody>
               </table>
            </div>
                  </div>
                </div>
              </div>

<script type="text/javascript" src="//code.jquery.com/jquery-1.5.2.js"></script>
<script>   
$(function(){
 $("#btn_enviar").click(function(){
 var url = "{{ url('/reporte-search') }}"; // El script a dónde se realizará la petición.
    $.ajax({
           type: "POST",
           url: url,
           data: $("#form").serialize(), // Adjuntar los campos del formulario enviado.
           success: function(data)
           {
               
            $("#datatable").empty();
            $("#resultado").empty();

               var totalr = data.length;
               
               if(totalr == 0)
               {
                 $('#resultado').append('<h3><i class="fa fa-file-o" aria-hidden="true"></i> Sin Resultados</h3>');
               }
               
               $.each(data, function(index) {
                    //alert(data[index].monto);
                    $('#datatable').append('<tr id="depositop" class="even pointer"><td class="text-center" width="20%">'+ data[index].referencia +'</td><td class="text-center" width="20%">'+ data[index].cargo +'</td><td class="text-center" width="10%">$'+ data[index].abono +'</td><td class="text-center" width="10%">'+ data[index].saldo +'</td><td class="text-center" width="10%">'+ data[index].fecha +'</td><td class="text-center" width="10%">'+ data[index].referencia2 +'</td></tr>');
               });
           }
         });

    return false; // Evitar ejecutar el submit del formulario.
 });
});
</script>

@endsection
