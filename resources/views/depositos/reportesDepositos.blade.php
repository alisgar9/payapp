@extends('layouts.logged')
@section('content')

<div class="col-md-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Mis Depositos</h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <br>
                    <p>Generar Reporte por Mes.</p>

                    <form id="form" method="post" class="form-vertical form-label-left">
                      {{ csrf_field() }}
                      <div class="form-group">
                        <div class="col-md-4">
                          <select name="mes" requerid class="form-control">
                              <option value="0">Selecciona una Opción</option>
                              <option value="1">Enero</option>
                              <option value="2">Febrero</option>
                              <option value="3">Marzo</option>
                              <option value="4">Abril</option>
                              <option value="5">Mayo</option>
                              <option value="6">Junio</option>
                              <option value="7">Julio</option>
                              <option value="8">Agosto</option>
                              <option value="9">Septiembre</option>
                              <option value="10">Octubre</option>
                              <option value="11">Noviembre</option>
                              <option value="12">Diciembre</option>
                          </select>
                        </div>
                        <div class="col-md-4">
                          <button type="button" id="btn_enviar" class="btn btn-success">Generar</button>
                        </div>
                      </div>
                    </form>
                    
            <div class="table-responsive">
            <table id="datatable2" class="table table-striped table-bordered dataTable no-footer" role="grid" aria-describedby="datatable_info">
                  <thead>
                     <tr class="headings" align="center">
                        <th class="column-title" width="20%" style="display: table-cell;">Forma de Pago</th>
                        <th class="column-title" width="15%" style="display: table-cell;"># Deposito </th>
                        <th class="column-title" width="10%" style="display: table-cell;">Monto Deposito</th>
                        <th class="column-title" width="10%" style="display: table-cell;">Banco </th>
                        <th class="column-title" width="10%" style="display: table-cell;">Factura</th>
                        <th class="column-title" width="10%" style="display: table-cell;">Fecha Deposito</th>
                        <th class="column-title" width="10%" style="display: table-cell;">Hora Deposito</th>
                        <th class="column-title" width="15%" style="display: table-cell;">Estado</th>
                     </tr>
                  </thead>
            </table>
            <div class="col-md-12 text-center" id="resultado"></div>
            <table id="datatable" class="table table-striped table-bordered dataTable no-footer" role="grid" aria-describedby="datatable_info">
                  <tbody>
                     @foreach ($depositos as $bm)
                     <tr class="even pointer">
                        <td class="text-center" width="20%" >{{$bm->nombre}}</td>
                        <td class="text-center" width="15%" >{{$bm->numero_deposito}}</td>
                        <td class="text-center" width="10%" >${{$bm->monto_deposito}}</td>
                        <td class="text-center" width="10%" >{{$bm->banco_deposito}}</td>
                        <td class="text-center" width="10%" >@if($bm->factura == 0)
                             <a href="#" class="btn btn-first" alt="Por Aprobar">
                              <i class="fa fa-ban" data-userway-font-size="18"></i> No
                             </a>
                          @else
                             <a href="#" class="btn btn-first" alt="Por Aprobar">
                              <i class="fa fa-check" data-userway-font-size="18"></i> Si
                             </a>
                          @endif   
                        </td>
                        <td class="text-center" width="10%" >{{ Carbon\Carbon::parse($bm->deposito_fecha)->format('Y-m-d') }}</td>
                        <td class="text-center" width="10%" >{{$bm->deposito_hora}}</td>
                        <td class="text-center" width="15%" >@if($bm->status == 0)
                             <a href="#" class="btn btn-first" alt="Por Aprobar">
                              <i class="fa fa-clock-o" data-userway-font-size="18"></i> Por Aprobar
                             </a>
                          @elseif($bm->status == 1)
                             <a href="#" class="btn btn-first" alt="Aprobado">
                              <i class="fa fa-check" data-userway-font-size="18"></i> Aprobado
                             </a>
                          @elseif($bm->status == 2)
                             <a href="#" class="btn btn-first" alt="Cancelado">
                              <i class="fa fa-ban" data-userway-font-size="18"></i> Cancelado

                             </a> 
                             fa-ban  
                          @endif   
                        </td>
                     </tr>
                     @endforeach
                      
                  </tbody>
               </table>
            </div>
                  </div>
                </div>
              </div>

<script type="text/javascript" src="//code.jquery.com/jquery-1.5.2.js"></script>
<script>   
$(function(){
 $("#btn_enviar").click(function(){
 var url = "{{ url('/deposito-search') }}"; // El script a dónde se realizará la petición.
    $.ajax({
           type: "POST",
           url: url,
           data: $("#form").serialize(), // Adjuntar los campos del formulario enviado.
           success: function(data)
           {
               
            $("#datatable").empty();
            $("#resultado").empty();

               var totalr = data.length;
               
               if(totalr == 0)
               {
                 $('#resultado').append('<h3><i class="fa fa-file-o" aria-hidden="true"></i> Sin Resultados</h3>');
               }

               $.each(data, function(index) {
                    //alert(data[index].monto);
                    
                    var factura ='';
                    if (data[index].factura == 0){
                      factura = '<a href="#" class="btn btn-first" alt="Por Aprobar"><i class="fa fa-ban" data-userway-font-size="18"></i> No </a>';
                    }else{
                      factura ='<a href="#" class="btn btn-first" alt="Por Aprobar"><i class="fa fa-check" data-userway-font-size="18"></i> Si </a>';
                    }

                    var status = '';

                    if(data[index].status == 0){
                          status = '<a href="#" class="btn btn-first" alt="Por Aprobar"><i class="fa fa-clock-o" data-userway-font-size="18"></i> Por Aprobar</a>';
                    }
                    else if(data[index].status == 1){
                          status = '<a href="#" class="btn btn-first" alt="Aprobado"><i class="fa fa-check" data-userway-font-size="18"></i> Aprobado</a>';
                    }
                    else if(data[index].status == 2){
                          status ='<a href="#" class="btn btn-first" alt="Cancelado"><i class="fa fa-ban" data-userway-font-size="18"></i> Cancelado</a>'; 
                    }
                             


                    $('#datatable').append('<tr id="depositop" class="even pointer"><td class="text-center">'+ data[index].nombre +'</td><td class="text-center">'+ data[index].numero_deposito +'</td><td class="text-center">$'+ data[index].monto_deposito +'</td><td class="text-center">'+ data[index].banco_deposito +'</td><td class="text-center">'+ factura +'</td><td class="text-center">'+ data[index].deposito_fecha +'</td><td class="text-center">'+ data[index].deposito_hora +'</td><td>'+ status +'</td></tr>');
               });
           }
         });

    return false; // Evitar ejecutar el submit del formulario.
 });
});
</script>

@endsection
