<!DOCTYPE html>
<html lang="en">
   <head>
      <meta http-equiv="Expires" content="0" /> 
      <meta http-equiv="Pragma" content="no-cache" />
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <title>PayApp ©  2018</title>
      <link href="{{asset('vendors/bootstrap/dist/css/bootstrap.min.css')}}" rel="stylesheet">
      <link href="{{asset('vendors/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">
      <link href="{{asset('vendors/nprogress/nprogress.css')}}" rel="stylesheet">
      <link href="{{asset('vendors/iCheck/skins/flat/green.css')}}" rel="stylesheet">
      <link href="{{asset('vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css')}}" rel="stylesheet">
      <link href="{{asset('vendors/jqvmap/dist/jqvmap.min.css')}}" rel="stylesheet"/>
      <link href="{{asset('vendors/bootstrap-daterangepicker/daterangepicker.css')}}" rel="stylesheet">
      <link rel="stylesheet" type="text/css" href="{{asset('js/datatables/datatables.min.css')}}"/>
      <link rel="stylesheet" href="{{asset('vendors/easyautocomplete/easy-autocomplete.css')}}">
      <link rel=“stylesheet” href="{{asset('vendors/easyautocomplete/easy-autocomplete.themes.min.css')}}">
      <link href="https://cdn.rawgit.com/harvesthq/chosen/gh-pages/chosen.min.css" rel="stylesheet"/>
      <link href="{{asset('vendors/pnotify/dist/pnotify.css')}}" rel="stylesheet">
      <link href="{{asset('vendors/pnotify/dist/pnotify.buttons.css')}}" rel="stylesheet">
      <link href="{{asset('vendors/pnotify/dist/pnotify.nonblock.css')}}" rel="stylesheet">
      <link href="{{asset('build/css/custom.min.css')}}" rel="stylesheet">
      <script type="text/javascript">
        if(history.forward(1)){
          location.replace( history.forward(1) );
        }
      </script>
   </head>
   <body>   
<br/><br/><br/><br/>
<hr class="estilo">
<div class="text-center col-md-12" style="background-color: #f4f4f4;color:#000000;" />
  <div class="text-center col-md-6">
    <br><br><br>
    <img src="images/check.png" width="60%"><br /><br /><br />
  </div>
  <div class="text-center col-md-6">
    <br/><br/><br/><br/><br/><br/>
    <h1><i class="fa fa-exclamation-triangle"></i> Su pago esta en Revision!!</h1>
    <h2>Tu depòsito a quedado reportado, esta siendo validado.</h2>
    <h2>En unos Minutos te Notificaremos.</h2><br /><br />
    <a href="{{ url('/home')}}"><button name="button" class="boton-stylo">Regresar al Panel</button></a>
  </div>  
</div>  
</body>
</html>

<style media="screen">

  .margen{
    width: 200px;
    height: 25px;
    margin: 15px;}

  div > input:focus{
    background: #0cac00;
    border: 0;}

  .boton-stylo{
    width: 200px;
    font-size: 17px;
    height: 40px;
    cursor: pointer;
    background-color: #0cac00;
    border: 0;
    color: #fff;}

  .boton-stylo:hover{
    background: black;
    box-shadow: 2px 3px 4px 1px #bfbfbf;}


      body{
        padding: 0px 65px;
      }
      .mayus{
        text-transform: uppercase;}
      .titulo{
        display: flex;
        justify-content: space-around;}
      .estilo{
        border:#0cac00 5px solid;}
      .titulos{
        color:#0cac00;}
      .texto{
        display: flex;}
      .estilo-inputo-dos:hover{
        border: #0cac00 1px solid;
      }
    </style>
