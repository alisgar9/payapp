@extends('layouts.logged')
@section('content')

<div class="col-md-12 col-sm-12 col-xs-12">
   <div class="page-title">
      <div class="title_left">
         <h3>Recargas Electronicas</h3>
      </div>
      <div class="title_right text-right">
         <h3>Total de Saldo: $<?php echo number_format((float)$saldo->saldo, 2, '.', ''); ?></h3>
      </div>
   </div>
   <form role="form" method="POST" id="form" onsubmit="openModal()" />
          {{ csrf_field() }}

          @if(session()->has('field_errors'))
                <div class="col-md-12 col-xs-12 w3-panel w3-red w3-display-container">
                  <span onclick="this.parentElement.style.display='none'" class="w3-button w3-red w3-large w3-display-topright">×</span>
                  <h3>Datos Requeridos!</h3>
                    @foreach (session()->get('field_errors')->all() as $error)
                    <div>{{ $error }}</div>
                  @endforeach
                </div>
              @endif

              @if(session()->has('message'))
                    <div class="alert alert-danger">
                        {{ session()->get('message') }}
                    </div>
                @endif

   <div class="col-md-4 col-xs-12">
      <div class="x_panel">
         <div class="x_title">
            <h2>Selecciona la Compañia</h2>
         </div>
         <div class="x_content">
            <br>
            <div class="form-group">
               <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                 @foreach ($companias as $com)
                   <div class="col-md-6 col-xs-12 text-center">
                       <label>
                       <input type="radio" id="compania" name="compania" value="{{$com->idcompania}}" class="sr-only" required />
                       <img src="<?php echo url($com->imagen); ?>">
                       </label>
                    </div>
                 @endforeach
                  
                  
               </div>
            </div>
         </div>
      </div>
   </div>
   <div class="col-md-4 col-xs-12" id="monto">
      <div class="x_panel">
         <div class="x_title">
            <h2>Selecciona el Monto</h2>
         </div>
         <div class="x_content">
            <br>
            <div class="form-group text-center">
               <ul class="chec-radio" id='montolist'>
              
               </ul>
            </div>
         </div>
      </div>
    </div>
      <div class="col-md-4 col-xs-12" id="phones">
        <div class="x_panel">
         <div class="x_title">
            <h2>Número Destinatario</h2>
         </div>
         <div class="x_content">
          <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
            <label>Compañia</label>
              <input type="text" class="form-control" id="textCompania" name="textCompania" readonly placeholder="telcel">
          </div>
          <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
            <label>Monto</label>
              <input type="text" class="form-control" id="textMonto" name="textMonto" readonly placeholder="500.00">
          </div>
          <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback" id="desc">
          </div>  

          <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                        <input type="tel" class="form-control" id="phone1" style="height: 80px;font-size: 28px; font-weight: bold;" required name="phone" placeholder="5512345678" autocomplete="off" title="El Número Destinatario debe contener 10 digitos."  >
                        <span class="fa fa-phone form-control-feedback right" pattern="[0-9]{10}" aria-hidden="true" tabindex="1"></span>
                      </div>
                       <div class="col-md-12 col-sm-6 col-xs-12 form-group has-feedback">
                        <input type="tel" autocomplete="off" style="height: 80px;font-size: 28px; font-weight: bold;" pattern="[0-9]{10}" class="form-control" id="phone2" required name="phone" placeholder="5512345678" tabindex="2" title="El Número Destinatario debe contener 10 digitos." />
                        <span class="fa fa-phone form-control-feedback right" aria-hidden="true"></span>
                      </div>          
          <button class="btn btn-success btn-lg pull-right" type="submit">Enviar</button>
         </div>
        </div>  
      </div>  
    </form>


<div class="modal fade" tabindex="-1" role="dialog" id="myModal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Recargas Electronicas</h4>
      </div>
      <div class="modal-body">
         <div class="x_content">
            <br>
            <form role="form" method="POST" id="form" action="{{ url('/recargas-add') }}">
               {{ csrf_field() }}
            <div class="form-group">
              <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                <label>Compañia</label>
                  <input type="text" class="form-control" id="textCompaniad" readonly placeholder="telcel" style="height: 80px;font-size: 28px; font-weight: bold;">
              </div>
              <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                <label>Monto</label>
                  <input type="text" class="form-control" id="textMontod" readonly placeholder="500.00" style="height: 80px;font-size: 28px; font-weight: bold;">
              </div>
              <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                <label>Número Destinatario</label>
                  <input type="text" class="form-control" id="textnumerod" readonly placeholder="500.00" style="height: 80px;font-size: 28px; font-weight: bold;">
              </div>
               <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                 <label>NIP de Seguridad</label>
                 <input type="password" class="form-control" id="textNip" name="textNip"  autocomplete="off" autocomplete="off" required style="height: 80px;font-size: 28px;font-weight: bold;" />
               </div>
               <div id="hiddentab"></div>
            </div>
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-primary" id="recargamosSend">Recargar</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
      </div>
    </form>

    <div class="col-md-12 col-xs-12">
   <div class="form-group" id="loadingg">
     <div class="col-md-12 col-sm-12 col-xs-12 text-center">
        <img src="{{asset('images/loading.gif')}}" width="30%"><br>
        <h3>Espere un Momento...</h3>
     </div>
  </div>    
    <div id="loading-img"></div>
  </div>             
</div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->




             
</div>

<style type="text/css">
  label > input{ /* HIDE RADIO */
  display:none;
}
label > input + img{ /* IMAGE STYLES */
  cursor:pointer;
  border:2px solid transparent;
  width: 100%;
}
label > input:checked + img{ /* (CHECKED) IMAGE STYLES */
  border:2px solid #dcd4d4;
   -webkit-filter: none;
       -moz-filter: none;
            filter: none;
            -webkit-box-shadow: 2px 2px 5px #999;
  -moz-box-shadow: 2px 2px 5px #999;
}
label > input:checked + img:hover{
   /* -webkit-filter: brightness(1) grayscale(3) opacity(0.5);*/
      
}
.btn span.glyphicon {         
  opacity: 0;       
}
.btn.active span.glyphicon {        
  opacity: 1;       
}

input:required,
textarea:required {
  border-color: red !important;
}
h5{
  font-size: 18px;
}
.error
{
color:red;
font-family:verdana, Helvetica;
}
#monto, #phones {
  display: none;
}

ul.chec-radio {
    
}
ul.chec-radio li.pz {
    /*display: inline;*/
    list-style-type: none;
}
.chec-radio label.radio-inline input[type="checkbox"] {
    display: none;
}
.chec-radio label.radio-inline input[type="checkbox"]:checked+div {
    color: #fff;
    background-color: #000;
}
.chec-radio .radio-inline .clab {
    cursor: pointer;
    background: #5bc0de;
    padding: 12px 30px;
    text-align: center;
    text-transform: uppercase;
    color: #333;
    width: 200px;
    position: relative;
    height: 45px;
    float: left;
    margin: 0;
    margin-bottom: 5px;
    border-color: #46b8da;
}
.chec-radio label.radio-inline input[type="checkbox"]:checked+div:before {
    content: "\e013";
    margin-right: 5px;
    font-family: 'Glyphicons Halflings';
}
.chec-radio label.radio-inline input[type="radio"] {
    display: none;
}
.chec-radio label.radio-inline input[type="radio"]:checked+div {
    color: #fff;
    background-color: #000;
}
.chec-radio label.radio-inline input[type="radio"]:checked+div:before {
    content: "\e013";
    margin-right: 5px;
    font-family: 'Glyphicons Halflings';
}

.w3-red, .w3-hover-red:hover {
    color: #fff!important;
    background-color: #f44336!important;
}
.w3-panel {
    margin-top: 16px;
    margin-bottom: 16px;
}
.w3-container, .w3-panel {
    padding: 0.01em 16px;
}
.w3-tooltip, .w3-display-container {
    position: relative;
}

#loading-img {
    display: none;
    height: 50px;
    width: 50px;
    position: absolute;
    top: 33%;
    left: 1%;
    right: 1%;
    margin: auto;
}
#loadingg {
  display: none;
}
</style>

<script type="text/javascript" src="//code.jquery.com/jquery-1.5.2.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/numeric/1.2.6/numeric.js"></script>
<script type="text/javascript">



  $(document).ready(function() {
    $('#form').attr('autocomplete', 'off');

    $('#phone1').on('input', function () { 
        this.value = this.value.replace(/[^0-9]/g,'');
    });

    $('#phone2').on('input', function () { 
        this.value = this.value.replace(/[^0-9]/g,'');
    });

    $(document).keydown(function(event) {
        if (event.ctrlKey==true && (event.which == '118' || event.which == '86')) {
            alert('thou. shalt. not. PASTE!');
            event.preventDefault();
         }
    });


    $("input:radio[name$='compania']").click(function() {
        var test = $(this).val();
        var url = "{{ url('/monto/') }}";
          
        $.ajax({  
          url:url,  
          method:"POST",  
          data:{
            "_token": "{{ csrf_token() }}",
            "id":test},  
          success:function(data){ 
            if (data.length === 0) {
                $('#error_message').html("Error");
            }else{
                $("#monto").show();
                $("input:radio[name$='compania']").click(function() {
                  $('#montolist').empty();
                  $('#hiddentab').empty();



                });
                $("#mentos").focus(); 
                $.each(data, function(index) {
                    //alert(data[index].monto);
                    $('#montolist').append('<li id="mentos" class="pz"><label class="radio-inline"><input type="radio" id="mento" name="mento" tabindex="-1" class="pro-chx" value="'+ data[index].id +'"><div class="clab">$'+ data[index].monto +'&nbsp;&nbsp; </div></label></li>');
                });

                $("input:radio[name$='mento']").click(function() {
                  var montot = $(this).val();
                  $("#phones").show();
                  $('#desc').empty();
                  $("#phone1").focus();

                    $.ajax({  
                        url:"{{ url('/monto-view/') }}",  
                        method:"POST",  
                        data:{
                        "_token": "{{ csrf_token() }}",
                        "companiaview":test,
                        "montoview":montot},  
                        success:function(data){  
                             console.log(data.nombre);
                             $("#textCompania").val(data.nombre);
                             $("#textCompaniad").val(data.nombre);  
                             $("#textMonto").val(data.monto);
                             $("#textMontod").val(data.monto);
                             $('#desc').append(data.descripcion);
                              var phone1, phone2;

                              phone1 = document.getElementById('phone1');
                              phone2 = document.getElementById('phone2');

                              phone1.onchange = phone2.onkeyup = passwordMatch; 

                              function passwordMatch() {
                                  if(phone1.value !== phone2.value)
                                      phone2.setCustomValidity('Los Numeros no Coinciden.');
                                  else
                                      phone2.setCustomValidity('');
                              }

                              var input=  document.getElementById('phone1');
                              input.addEventListener('input',function(){
                                if (this.value.length > 10) 
                                   this.value = this.value.slice(0,10); 
                              })

                              var input2=  document.getElementById('phone2');
                              input2.addEventListener('input',function(){
                                if (this.value.length > 10) 
                                   this.value = this.value.slice(0,10); 
                              })



                              input.oninvalid = function(event) {
                                  event.target.setCustomValidity('El Número Destinatario debe contener 10 digitos.');
                              }

                              input2.oninvalid = function(event) {
                                  event.target.setCustomValidity('El Número Destinatario de confirmacion debe contener 10 digitos.');
                              }


                              $('#form').on('submit', function(e){
                                
                                var telefono_r = $("#phone1").val();
                                var compania = $('input:radio[name=compania]:checked').val();
                                var monto = $('input:radio[name=mento]:checked').val()
                                $("#textnumerod").val(telefono_r);
                                $('#myModal').modal('show');
                                $('#hiddentab').append('<input type="hidden" name="pania" id="pania" value="'+ compania +'" /><input type="hidden" name="otnom" id="otnom" value="'+ monto +'" /><input type="hidden" name="enohp" id="enohp" value="'+ telefono_r +'" />');
                                e.preventDefault();
                              });

                        }  
                   });  
                });

            }    
                      
          }  
        });



        $(".press").keypress(function(event){
          if(event.which == 13){
          cb = parseInt($(this).attr('tabindex'));
          if ( $(':input[tabindex=\'' + (cb + 1) + '\']') != null) {
          $(':input[tabindex=\'' + (cb + 1) + '\']').focus();
          $(':input[tabindex=\'' + (cb + 1) + '\']').select();
          return false;
          }
          }
          });


        $("#recargamosSend").click(function() {
            var nip;
            nip = document.getElementById('textNip');
            if($("#textNip").val().length > 0){
                $("#form").hide();
                $("#loadingg").show();
                $(".container").css("opacity", 0.2);
                $("#loading-img").css({"display": "block"});
                setTimeout(function(){
                            $(".container").css("opacity", 1);
                      $("#loading-img").css({"display": "none"});
                },3000); 
            }                   
        });  

        //$("#textCompania").val(nombre);
    });

    
   

    


});
  $('.tg').bind('keypress', function(event) {
  if(event.which === 13) {
    var nextItem = $(this).next('.tg');

    if( nextItem.size() === 0 ) {
      nextItem = $('.tg').eq(0);
    }
    nextItem.focus();
  }
});

  function InvalidMsg(textbox) {
    
    if (textbox.value == '') {
        textbox.setCustomValidity('Required email address');
    }
    else if(textbox.validity.typeMismatch){
        textbox.setCustomValidity('please enter a valid email address');
    }
    else {
        textbox.setCustomValidity('');
    }
    return true;
}

</script>
@endsection
