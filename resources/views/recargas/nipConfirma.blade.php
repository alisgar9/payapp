@extends('layouts.logged')
@section('content')
<link href="{{asset('build/load/loading.css')}}" rel="stylesheet">
<div class="col-md-12 col-sm-12 col-xs-12">
   <div class="page-title">
      <div class="title_left">
         <h3>Recargas Electronicas</h3>
      </div>
   </div>
<div class="group">
    <div class="container">
   <form role="form" method="POST" id="recargasf" action="{{ url('/add-recargar') }}">
          @if( $message == 1 )
                    <div class="alert alert-danger">
                       <h1>NIP Incorrecto. </h1>
                    </div>
          @endif
          
          {{ csrf_field() }}

          @if(session()->has('field_errors'))
                <div class="col-md-12 col-xs-12 w3-panel w3-red w3-display-container">
                  <span onclick="this.parentElement.style.display='none'" class="w3-button w3-red w3-large w3-display-topright">×</span>
                  <h3>Datos Requeridos!</h3>
                    @foreach (session()->get('field_errors')->all() as $error)
                    <div>{{ $error }}</div>
                  @endforeach
                </div>
          @endif
           
   <div class="col-md-6 col-xs-12">
      <div class="x_panel">
         <div class="x_content">
            <br>

            <div class="form-group">
              <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                <label>Compañia</label>
                  <input type="text" class="form-control" value="{{$readonlys['compania']}}" id="textCompania" readonly placeholder="telcel">
              </div>
              <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                <label>Monto</label>
                  <input type="text" class="form-control" value="${{$readonlys['monto']}}" id="textMonto" readonly placeholder="500.00">
              </div>
              <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                <label>Número Destinatario</label>
                  <input type="text" class="form-control" id="textnumero" value="{{$readonlys['telefono']}}" readonly placeholder="500.00">
              </div>
               <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                 <label>NIP de Seguridad</label>
                 <input type="password" class="form-control" id="textNip" name="textNip"  autocomplete="off" autocomplete="off" required style="height: 65px;font-size: 24px;" />
                 <input type="hidden" name="data" value="{{$data}}">
                 <input type="hidden" name="lectura" value="{{$readonlyss}}">
                 <br/>
                 <a href="{{ url('/recargas') }}" class="btn btn-success btn-lg pull-left">Cancelar</a>
                 <button class="btn btn-success btn-lg pull-right" id="recarga" type="submit">Recarga</button>
                 <button id="id01" type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal"><i class="fa fa-print" aria-hidden="true"></i>
 Imprimir Ticket</button>
               </div>
            </div>
         </div>
      </div>
   </div>
   


  

    </form>
 </div>

</div>
<div class="col-md-12 col-xs-12">
   <div class="form-group" id="loadingg">
     <div class="col-md-12 col-sm-12 col-xs-12 text-center">
        <img src="{{asset('images/loading.gif')}}" width="30%"><br>
        <h3>Espere un Momento...</h3>
     </div>
  </div>    
    <div id="loading-img"></div>
  </div>             
</div>

<style type="text/css">
  label > input{ /* HIDE RADIO */
  display:none;
}
label > input + img{ /* IMAGE STYLES */
  cursor:pointer;
  border:2px solid transparent;
  -webkit-filter: brightness(1) grayscale(2) opacity(0.5);
      
            width: 100%;
}
label > input:checked + img{ /* (CHECKED) IMAGE STYLES */
  border:2px solid #dcd4d4;
   -webkit-filter: none;
       -moz-filter: none;
            filter: none;
            -webkit-box-shadow: 2px 2px 5px #999;
  -moz-box-shadow: 2px 2px 5px #999;
}
label > input:checked + img:hover{
   /* -webkit-filter: brightness(1) grayscale(3) opacity(0.5);*/
      
}
.btn span.glyphicon {         
  opacity: 0;       
}
.btn.active span.glyphicon {        
  opacity: 1;       
}

input:required,
textarea:required {
  border-color: red !important;
}
h5{
  font-size: 18px;
}
.error
{
color:red;
font-family:verdana, Helvetica;
}
#monto, #phones {
  display: none;
}

ul.chec-radio {
    
}
ul.chec-radio li.pz {
    /*display: inline;*/
    list-style-type: none;
}
.chec-radio label.radio-inline input[type="checkbox"] {
    display: none;
}
.chec-radio label.radio-inline input[type="checkbox"]:checked+div {
    color: #fff;
    background-color: #000;
}
.chec-radio .radio-inline .clab {
    cursor: pointer;
    background: #5bc0de;
    padding: 12px 30px;
    text-align: center;
    text-transform: uppercase;
    color: #333;
    width: 200px;
    position: relative;
    height: 45px;
    float: left;
    margin: 0;
    margin-bottom: 5px;
    border-color: #46b8da;
}
.chec-radio label.radio-inline input[type="checkbox"]:checked+div:before {
    content: "\e013";
    margin-right: 5px;
    font-family: 'Glyphicons Halflings';
}
.chec-radio label.radio-inline input[type="radio"] {
    display: none;
}
.chec-radio label.radio-inline input[type="radio"]:checked+div {
    color: #fff;
    background-color: #000;
}
.chec-radio label.radio-inline input[type="radio"]:checked+div:before {
    content: "\e013";
    margin-right: 5px;
    font-family: 'Glyphicons Halflings';
}

.w3-red, .w3-hover-red:hover {
    color: #fff!important;
    background-color: #f44336!important;
}
.w3-panel {
    margin-top: 16px;
    margin-bottom: 16px;
}
.w3-container, .w3-panel {
    padding: 0.01em 16px;
}
.w3-tooltip, .w3-display-container {
    position: relative;
}

#loading-img {
    display: none;
    height: 50px;
    width: 50px;
    position: absolute;
    top: 33%;
    left: 1%;
    right: 1%;
    margin: auto;
}
.container{
    /*height: 150px;
    background: #000;
    color: #fff;*/
}
.group {
    position: relative;
    width: 70%;
}
#loadingg {
  display: none;
}
</style>

<script type="text/javascript" src="//code.jquery.com/jquery-1.5.2.js"></script>
<script type="text/javascript">
$("#recarga").click(function() {
    var nip;
    nip = document.getElementById('textNip');
    if($("#textNip").val().length > 0){
        $("#recargasf").hide();
        $("#loadingg").show();
        $(".container").css("opacity", 0.2);
        $("#loading-img").css({"display": "block"});
        setTimeout(function(){
                    $(".container").css("opacity", 1);
              $("#loading-img").css({"display": "none"});
        },3000); 

    }
           
});
</script>
@endsection
