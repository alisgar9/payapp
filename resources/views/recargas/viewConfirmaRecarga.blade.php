
<!DOCTYPE html>
<html lang="en">
   <head>
      <meta http-equiv="Expires" content="0" /> 
      <meta http-equiv="Pragma" content="no-cache" />
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <title>PayApp ©  2018</title>
      <link href="{{asset('vendors/bootstrap/dist/css/bootstrap.min.css')}}" rel="stylesheet">
      <link href="{{asset('vendors/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">
      <link href="{{asset('vendors/nprogress/nprogress.css')}}" rel="stylesheet">
      <link href="{{asset('vendors/iCheck/skins/flat/green.css')}}" rel="stylesheet">
      <link href="{{asset('vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css')}}" rel="stylesheet">
      <link href="{{asset('vendors/jqvmap/dist/jqvmap.min.css')}}" rel="stylesheet"/>
      <link href="{{asset('vendors/bootstrap-daterangepicker/daterangepicker.css')}}" rel="stylesheet">
      <link rel="stylesheet" type="text/css" href="{{asset('js/datatables/datatables.min.css')}}"/>
      <link rel="stylesheet" href="{{asset('vendors/easyautocomplete/easy-autocomplete.css')}}">
      <link rel=“stylesheet” href="{{asset('vendors/easyautocomplete/easy-autocomplete.themes.min.css')}}">
      <link href="https://cdn.rawgit.com/harvesthq/chosen/gh-pages/chosen.min.css" rel="stylesheet"/>
      <link href="{{asset('vendors/pnotify/dist/pnotify.css')}}" rel="stylesheet">
      <link href="{{asset('vendors/pnotify/dist/pnotify.buttons.css')}}" rel="stylesheet">
      <link href="{{asset('vendors/pnotify/dist/pnotify.nonblock.css')}}" rel="stylesheet">
      <link href="{{asset('build/css/custom.min.css')}}" rel="stylesheet">
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

      <!-- Latest compiled JavaScript -->
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

      <script type="text/javascript">
      function redireccionar(){
        document.getElementById('id01').style.display='active';
        $("#id01").trigger("click");
      } 
      setTimeout ("redireccionar()", 1000); //tiempo expresado en milisegundos
      </script>

      <script type="text/javascript">
        if(history.forward(1)){
          location.replace( history.forward(1) );
        }
      </script>
   </head>
   <body>   
<br/><br/><br/><br/>
<hr class="estilo">
<div class="text-center col-md-12" style="background-color: #f4f4f4;color:#000000;" />
  <div class="text-center col-md-6">
    <br><br><br>
    @if($status == '2')
    <img src="{{asset('images/stop.png')}}" width="60%">
    @elseif($status == '1')
    <img src="{{asset('images/if_phoneok.png')}}" width="60%">
    <br /><br /><br />
    @endif
  </div>
  <div class="text-center col-md-6">
    <br/><br/><br/><br/><br/><br/>
    @if($descripcion == '01')
     <h1><i class="fa fa-exclamation-triangle"></i> Teléfono no perteneciente a esta compania</h1>
    @elseif($descripcion == '03')
     <h1><i class="fa fa-exclamation-triangle"></i> Monto no valido</h1>
     @elseif($descripcion == '04')
     <h1><i class="fa fa-exclamation-triangle"></i> Telefono no suceptible de abono</h1>
     @elseif($descripcion == '05')
     <h1><i class="fa fa-exclamation-triangle"></i> Saldo Insuficiente</h1>
     @elseif($descripcion == '06')
     <h1><i class="fa fa-exclamation-triangle"></i> Region no permitida para venta</h1>
     @elseif($descripcion == '08')
     <h1><i class="fa fa-exclamation-triangle"></i> Timeout reintente</h1>
     @elseif($descripcion == '11')
     <h1><i class="fa fa-exclamation-triangle"></i> Medio de Venta no permitido</h1>
     @elseif($descripcion == '12')
     <h1><i class="fa fa-exclamation-triangle"></i>Producto no Catalogado</h1>
     @elseif($descripcion == '13')
     <h1><i class="fa fa-exclamation-triangle"></i> Lo sentimos, la venta del servicio no pudo realizarse</h1>
     @elseif($descripcion == '14')
     <h1><i class="fa fa-exclamation-triangle"></i> Se esta presentando lentitud, intente mas tarde</h1>
     @elseif($descripcion == '15')
     <h1><i class="fa fa-exclamation-triangle"></i> Recarga repetida en menos de un minuto</h1>
     @elseif($descripcion == '00')
     <h1><i class="fa fa-money"></i> Gracias por tu Recarga!!</h1>
     <div class="text-left col-md-12">
       <ul>
        <li><b>Numero de Recarga:</b> {{$phone}}</li>
        <li><b>Fecha:</b> {{$fecha}}</li>
        <li><b>Folio:</b> {{$folio}}</li>
       </ul>
     </div>
     <!-- <a href="/recarga-print/{{$data}}/{{$folio}}/{{$fecha}}"><button name="button" class="boton-stylo"><i class="fa fa-print" aria-hidden="true"></i>
 Imprimir Ticket</button></a> -->
 <button id="id01" type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal"><i class="fa fa-print" aria-hidden="true"></i>
 Imprimir Ticket</button>

    @endif
    <br><br>
    <a href="{{ url('/recargas')}}"><button name="button" class="boton-stylo"><i class="fa fa-mobile" aria-hidden="true"></i>
 Volver hacer Recarga</button></a>
    <br/><br/><br/><br/><br/><br/>
  </div>  
</div> 


<div id="myModal" class="modal fade" role="dialog">
  <div  class="modal-dialog">

    <!-- Modal content-->
    <div  class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Imprimir Ticket</h4>
      </div>
      <div id="id01" class="modal-body">
        <div class="embed-responsive embed-responsive-16by9">
  <iframe class="embed-responsive-item" src="{{ url('/recarga-print')}}/{{$data}}/{{$folio}}/{{$fecha}}" allowfullscreen></iframe>
</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
      </div>
    </div>

  </div>
</div>

</body>
</html>

<style media="screen">

  .margen{
    width: 200px;
    height: 25px;
    margin: 15px;}

  div > input:focus{
    background: #0cac00;
    border: 0;}

  .boton-stylo{
    width: 200px;
    font-size: 17px;
    height: 40px;
    cursor: pointer;
    background-color: #0cac00;
    border: 0;
    color: #fff;}

  .boton-stylo:hover{
    background: black;
    box-shadow: 2px 3px 4px 1px #bfbfbf;}


      body{
        padding: 0px 65px;
      }
      .mayus{
        text-transform: uppercase;}
      .titulo{
        display: flex;
        justify-content: space-around;}
      .estilo{
        border:#0cac00 5px solid;}
      .titulos{
        color:#0cac00;}
      .texto{
        display: flex;}
      .estilo-inputo-dos:hover{
        border: #0cac00 1px solid;
      }
    </style>
