<!DOCTYPE html>
<html lang="en">
   <head>
      <meta http-equiv="Expires" content="0" /> 
      <meta http-equiv="Pragma" content="no-cache" />
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <title>PayApp ©  2018</title>
      <link href="{{asset('vendors/bootstrap/dist/css/bootstrap.min.css')}}" rel="stylesheet">
      <link href="{{asset('build/css/print.css')}}" rel="stylesheet">
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/PrintArea/2.4.1/PrintArea.css" />
      <script type="text/javascript">
        if(history.forward(1)){
          location.replace( history.forward(1) );
        }
      </script>
      <style type="text/css" media="print">
			.no-print{ display: none; }
			@page{margin: 0;padding:0;}
		</style>
   </head>
   <body>
	<body>
		<div id="botones" class="no-print">
			<button data-impresion="" id="btnImprimir">Imprimir</button>
		</div>
		<div id="container">
			<div class="margin-left margin no-print"></div>
			<div class="margin-right margin no-print"></div>
            <div title="Tienda" class="absolute" id="tienda" style="left:38px;top:16px; text-align: center;font-size: 18px;"><b>
            	PROCOTEL SA DE CV</b></div>
            <div title="Dirección Fiscal" class="absolute" id="direccion fiscal" style="top:52px; text-align: center; left:38px;">
                R.F.C. PCT061025NU0<br>
                Insurgentes Sur 1647 <br>
                Col San José Insurgentes, Cd. México<br> 03900<br>
                Tel. 5642.1961
            </div>
            <div title="folio" class="absolute" id="serie" style="left:38px;top:131px;">Folio: {{$folio}}</div>
			<div title="Fecha de Emisión" class="absolute" id="fecha" style="left:38px;top:154px;">Fecha: {{$fecha}}</div>
			<div title="Detalle del Comprobante"  style="left:38px;top:200px;" class="absolute" id="detalle">
				<div class="col-md-12">
				<b>Teléfono de Recarga:</b> {{$phone}}<br>
				</div>
				<div class="col-md-12">
				<b>Compañia:</b> {{$compania}}<br>
				</div>
				
				<div class="col-md-12">
					<b>Total:</b> $<?php echo number_format($monto, 2); ?></br>
					<b>{{$montol}} Pesos 00/100 M.N.</b>
				</div>
				
				
<hr>
					
			</div>
            <div class="absolute" style="left:38px;top:270px;font-size: 12px;" >
					***Telcel
					PARA ABONO DIRECTO TELCEL

					PARA DUDAS O ACLARACIONES DE TU

					RECARGA MARCA *264 DE TU CEL 0

					01800 7105687
				</div>
		</div>

	</body>
	<script type="text/javascript" src="//code.jquery.com/jquery-1.5.2.js"></script>
	<script src="{{asset('vendors/bootstrap/dist/js/bootstrap.min.js')}}"></script>

	<script>
		$("#btnImprimir").click(function(){
					var f = '';

					f += '#fecha?' + $("#fecha").attr('style') + '|';
					f += '#cliente?' + $("#cliente").attr('style') + '|';
					f += '#ruc?' + $("#ruc").attr('style') + '|';
					f += '#direccion?' + $("#direccion").attr('style') + '|';
					f += '#serie?' + $("#serie").attr('style') + '|';
					f += '#SubTotal?' + $("#SubTotal").attr('style') + '|';
					f += '#total?' + $("#total").attr('style') + '|';
					f += '#TotalLetras?' + $("#TotalLetras").attr('style') + '|';
					f += '#IvaTotal?' + $("#IvaTotal").attr('style') + '|';
					f += '#Iva?' + $("#Iva").attr('style') + '|';
					f += '#detalle?' + $("#detalle").attr('style') + '|';
					f += '#detalle .row?';
					
					$('#detalle .row').each(function(){
						f += $(this).attr('style') + '!';
					})
					
					if($('#detalle .row').size() > 0)
					{
						f = f.substring(0,f.length - 1);
					}
					PrepararHoja();
								window.print();
				})
			function PrepararHoja()
			{
				$(".hidden").hide();
				$("body, .absolute, .row").css('background', 'none');
				$(".row,#container").css('border', 'none');
			}
		</script>
</html>