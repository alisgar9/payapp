<!DOCTYPE html>
<html lang="en">
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <title>PayApp ©  2018</title>
      <link href="{{asset('vendors/bootstrap/dist/css/bootstrap.min.css')}}" rel="stylesheet">
      <link href="{{asset('vendors/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">
      <link href="{{asset('vendors/nprogress/nprogress.css')}}" rel="stylesheet">
      <link href="{{asset('vendors/iCheck/skins/flat/green.css')}}" rel="stylesheet">
      <link href="{{asset('vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css')}}" rel="stylesheet">
      <link href="{{asset('vendors/jqvmap/dist/jqvmap.min.css')}}" rel="stylesheet"/>
      <link href="{{asset('vendors/bootstrap-daterangepicker/daterangepicker.css')}}" rel="stylesheet">
      <link rel="stylesheet" type="text/css" href="{{asset('js/datatables/datatables.min.css')}}"/>
      <link rel="stylesheet" href="{{asset('vendors/easyautocomplete/easy-autocomplete.css')}}">
      <link rel=“stylesheet” href="{{asset('vendors/easyautocomplete/easy-autocomplete.themes.min.css')}}">
      <link href="https://cdn.rawgit.com/harvesthq/chosen/gh-pages/chosen.min.css" rel="stylesheet"/>
      <link href="{{asset('vendors/pnotify/dist/pnotify.css')}}" rel="stylesheet">
      <link href="{{asset('vendors/pnotify/dist/pnotify.buttons.css')}}" rel="stylesheet">
      <link href="{{asset('vendors/pnotify/dist/pnotify.nonblock.css')}}" rel="stylesheet">
      <link href="{{asset('build/css/custom.min.css')}}" rel="stylesheet">
      <script type="text/javascript">
         var _userway_config = {
         // uncomment the following line to override default position
         position: 5,
         // uncomment the following line to override default language (e.g., fr, de, es, he, nl, etc.)
          language: 'es',
         // uncomment the following line to override color set via widget
          //color: '#eed81e',
         account: 'WPGtFQ2cby'
         };
      </script>
      <script type="text/javascript" src="https://cdn.userway.org/widget.js"></script>
      <script type="text/javascript" src="https://cdn.userway.org/widget.js"></script>
   </head>
   <?php $user = Auth::id();   ?>
   <body class="nav-md">
      <div class="container body">
         <div class="main_container">
            <div class="col-md-3 left_col">
               <div class="left_col scroll-view">
                  <div class="navbar nav_title" style="border: 0;">
                     <a href="{{ url('/home')}}" class="site_title"><span>
                     <img src="{{asset('images/logo-payapp.png')}}" width="80%">
                     </span></a>
                  </div>
                  <div class="clearfix"></div>
                  <div class="profile clearfix">
                     <div class="profile_pic">
                        <img src="{{asset('images/img.jpg')}}" alt="..." class="img-circle profile_img">
                     </div>
                     <div class="profile_info">
                        <span>Bienvenido,</span>
                        <h2>{{ Auth::user()->cliente }}</h2>
                     </div>
                  </div>
                  <br/>
                  <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
                     <div class="menu_section">
                        <h3>MENÚ</h3>
                        <ul class="nav side-menu">
                        <li>
                           <a href="{{ url('recargas/')}}"><i class="fa fa-dollar"></i> Recargas Electronicas</a>
                           
                        </li>
                        
                        <li>
                           <a href="{{ url('servicios/')}}"><i class="fa fa-money "></i> Pago de Servicios</a>
                        </li>
                        <li>
                           <a href="{{ url('depositos/')}}"><i class="fa fa-edit "></i> Registar Deposito</a>
                        </li>
                        <li>
                           <a><i class="fa fa-line-chart"></i>Reportes <span class="fa fa-chevron-down"></span></a>
                           <ul class="nav child_menu">
                              <li><a href="{{ url('reporte-deposito/')}}">Reporte de Depositos</a></li>
                              <li><a href="{{ url('reporte-ven/')}}">Reporte de Ventas</a></li>
                              <li><a href="{{ url('depositos-rep/')}}">Estado de Cuenta</a></li>
                              
                           </ul>
                        </li>
                        <li>
                           <a href="{{ url('utilerias/')}}"><i class="fa fa-book"></i> Utilerias </a>
                        </li>
                        <li>
                           <a href="https://procotel.myshopify.com/" target="blank"><i class="fa fa-cart-plus"></i> Compra de Equipos Aquí </a>
                        </li>
                        <li>
                              <a href="{{ url('/logout') }}"><i class="fa fa-sign-out pull-right"></i> Cerrar mi sesión</a>
                        </li>
                     </div>
                  </div>
                  <!-- /sidebar menu -->
                  <!-- /menu footer buttons -->
                  <div class="sidebar-footer hidden-small">
                     <a data-toggle="tooltip" data-placement="top" title="Cerrar mi sesión" href="{{ url('/logout') }}">
                     <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
                     </a>
                  </div>
                  <!-- /menu footer buttons -->
               </div>
            </div>
            <!-- top navigation -->
            <div class="top_nav">

               <div class="nav_menu">

                  <nav>
                     <div class="nav toggle">
                        <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                        
                     </div>
                     <ul class="nav navbar-nav navbar-right">
                     <li class="">
                        <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                        <img src="{{asset('images/img.jpg')}}" alt="">
                        {{ Auth::user()->name }}
                        <span class=" fa fa-angle-down"></span>
                        </a>
                        <ul class="dropdown-menu dropdown-usermenu pull-right">
                           <li><a href="{{ url('/perfil-usuario/0') }}"> Mi Perfil </a></li>
                           <li><a href="{{ url('/contrasena/') }}"> Cambiar Contraseña</a></li>
                           <li>
                              <a href="{{ url('/logout') }}"><i class="fa fa-sign-out pull-right"></i> Cerrar mi sesión</a>
                           </li>
                        </ul>
                     </li>
                      
                  </nav>
               </div>
            </div>
            <!-- /top navigation -->
            <!-- page content -->
            <div class="right_col" role="main">
            @yield('content')
            </div>
            <!-- /page content -->
            <!-- footer content -->
            <footer>
            <div class="pull-right">
            PayApp © 2018
            </div>
            <div class="clearfix"></div>
            </footer>
            <!-- /footer content -->
         </div>
      </div>
     <!-- jQuery -->
    <script src="{{asset('vendors/jquery/dist/jquery.min.js')}}"></script>
    <!-- Bootstrap -->
    <script src="{{asset('vendors/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <!-- FastClick -->
    <script src="{{asset('vendors/fastclick/lib/fastclick.js')}}"></script>
    <!-- NProgress -->
    <script src="{{asset('vendors/nprogress/nprogress.js')}}"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="{{asset('vendors/moment/min/moment.min.js')}}"></script>
    <script src="{{asset('vendors/bootstrap-daterangepicker/daterangepicker.js')}}"></script>
    <!-- bootstrap-datetimepicker -->    
    <script src="{{asset('vendors/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js')}}"></script>
    <!-- Ion.RangeSlider -->
    <script src="{{asset('vendors/ion.rangeSlider/js/ion.rangeSlider.min.js')}}"></script>
    <!-- Bootstrap Colorpicker -->
    <script src="{{asset('vendors/mjolnic-bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js')}}"></script>
    <!-- jquery.inputmask -->
    <script src="{{asset('vendors/jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js')}}"></script>
    <!-- jQuery Knob -->
    <script src="{{asset('vendors/jquery-knob/dist/jquery.knob.min.js')}}"></script>
    <!-- Cropper -->
    <script src="{{asset('vendors/cropper/dist/cropper.min.js')}}"></script>

    <!-- Custom Theme Scripts -->
    <script src="{{asset('js/custom.min.js')}}"></script>
    
    <!-- Initialize datetimepicker -->
<script>
    $('#myDatepicker').datetimepicker();
    
    $('#myDatepicker2').datetimepicker({
        format: 'DD.MM.YYYY'
    });
    
    $('#myDatepicker3').datetimepicker({
        format: 'hh:mm A'
    });
    
    $('#myDatepicker4').datetimepicker({
        ignoreReadonly: true,
        allowInputToggle: true
    });

    $('#datetimepicker6').datetimepicker();
    
    $('#datetimepicker7').datetimepicker({
        useCurrent: false
    });
    
    $("#datetimepicker6").on("dp.change", function(e) {
        $('#datetimepicker7').data("DateTimePicker").minDate(e.date);
    });
    
    $("#datetimepicker7").on("dp.change", function(e) {
        $('#datetimepicker6').data("DateTimePicker").maxDate(e.date);
    });
</script>
<!--Start of Zendesk Chat Script-->
<script type="text/javascript">
window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
$.src="https://v2.zopim.com/?60JGa0n5tmXk7umRdYnRQ24GUrlkm01C";z.t=+new Date;$.
type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
</script>
<!--End of Zendesk Chat Script-->
  </body>
</html>