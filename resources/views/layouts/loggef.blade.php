<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>PayApp ©  2018</title>

    <!-- Bootstrap -->
    <link href="{{asset('vendors/bootstrap/dist/css/bootstrap.min.css')}}" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="{{asset('vendors/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">
    <!-- NProgress -->
    <link href="{{asset('vendors/nprogress/nprogress.css')}}" rel="stylesheet">
    <!-- bootstrap-daterangepicker -->
    <link href="{{asset('vendors/bootstrap-daterangepicker/daterangepicker.css')}}" rel="stylesheet">
    <!-- bootstrap-datetimepicker -->
    <link href="{{asset('vendors/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css')}}" rel="stylesheet">
    <!-- Ion.RangeSlider -->
    <link href="{{asset('vendors/normalize-css/normalize.css')}}" rel="stylesheet">
    <link href="{{asset('vendors/ion.rangeSlider/css/ion.rangeSlider.cs')}}s" rel="stylesheet">
    <link href="{{asset('vendors/ion.rangeSlider/css/ion.rangeSlider.skinFlat.css')}}" rel="stylesheet">
    <!-- Bootstrap Colorpicker -->
    <link href="{{asset('vendors/mjolnic-bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css')}}" rel="stylesheet">

    <link href="{{asset('vendors/cropper/dist/cropper.min.css')}}" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="{{asset('build/css/custom.min.css')}}" rel="stylesheet">
  </head>
  <?php $user = Auth::id();   ?>
  <body class="nav-md">
      <div class="container body">
         <div class="main_container">
            <div class="col-md-3 left_col">
               <div class="left_col scroll-view">
                  <div class="navbar nav_title" style="border: 0;">
                     <a href="{{ url('/home')}}" class="site_title"><span>
                     <img src="{{asset('images/logo-payapp.png')}}" width="80%">
                     </span></a>
                  </div>
                  <div class="clearfix"></div>
                  <div class="profile clearfix">
                     <div class="profile_pic">
                        <img src="{{asset('images/img.jpg')}}" alt="..." class="img-circle profile_img">
                     </div>
                     <div class="profile_info">
                        <span>Bienvenido,</span>
                        <h2>{{ Auth::user()->cliente }}</h2>
                     </div>
                  </div>
                  <br/>
                  <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
                     <div class="menu_section">
                        <h3>MENÚ</h3>
                        <ul class="nav side-menu">
                        <li>
                           <a href="{{ url('recargas/')}}"><i class="fa fa-edit "></i> Recargas Electronicas</a>
                           
                        </li>
                        <li>
                           <a href="{{ url('servicios/')}}"><i class="fa fa-money "></i> Pago de Servicios</a>
                        </li>
                        <li>
                           <a><i class="fa fa-edit"></i>Registar Deposito <span class="fa fa-chevron-down"></span></a>
                           <ul class="nav child_menu">
                              <li><a href="{{ url('depositos-add/')}}">Agregar</a></li>
                              <li><a href="{{ url('reportes-depo/')}}">Reportes</a></li>
                           </ul>
                        </li>
                        <li>
                           <a><i class="fa fa-edit"></i> Estado de Cuenta <span class="fa fa-chevron-down"></span></a>
                           <ul class="nav child_menu">
                              <li><a href="{{ url('proveedor/agregar-proveedor/')}}">Agregar</a></li>
                              <li><a href="#">Catálogo</a></li>
                           </ul>
                        </li>
                        <li>
                           <a><i class="fa fa-user"></i> Utilerias <span class="fa fa-chevron-down"></span></a>
                           <ul class="nav child_menu">
                              <li><a href="{{ url('supervisor/agregar-supervisor/')}}">Agregar</a></li>
                              <li><a href="{{ url('supervisor/list-supervisor/')}}">Catálogo</a></li>
                           </ul>
                        </li>
                     </div>
                  </div>
                  <!-- /sidebar menu -->
                  <!-- /menu footer buttons -->
                  <div class="sidebar-footer hidden-small">
                     <a data-toggle="tooltip" data-placement="top" title="Cerrar mi sesión" href="{{ url('/logout') }}">
                     <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
                     </a>
                  </div>
                  <!-- /menu footer buttons -->
               </div>
            </div>
            <!-- top navigation -->
            <div class="top_nav">
               <div class="nav_menu">
                  <nav>
                     <div class="nav toggle">
                        <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                     </div>
                     <ul class="nav navbar-nav navbar-right">
                     <li class="">
                        <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                        <img src="{{asset('images/img.jpg')}}" alt="">
                        {{ Auth::user()->name }}
                        <span class=" fa fa-angle-down"></span>
                        </a>
                        <ul class="dropdown-menu dropdown-usermenu pull-right">
                           @if(Auth::user()->hasRole('proveedor'))
                           <?php
                              $user = Auth::id();
                              $users = Auth::user();
                              $tipo = $users->tipo;
                               ?>
                           <li><a href="/proveedor/perfil/<?php echo  $user;?>"> Mi Perfil </a></li>
                           <?php if ($tipo == 1) {?>
                           <li><a href="/agregar-proveedor-moral-formato" target="_blank"> Acuse de registro al directorio</a></li>
                           <?php }else{ ?>
                           <li><a href="/agregar-proveedor-fisica-formato" target="_blank"> Acuse de registro al directorio</a></li>
                           <?php } ?>
                           @endif
                           <?php $users = Auth::id(); ?>
                           @if(Auth::user()->hasRole('administrador'))
                           <?php if ($users == 12 || $users == 15 ) {?>
                           <li><a href="{{ url('cotizador/listado-proveedores')}}"> Listado de Proveedores</a></li>
                           <?php } ?>
                           @endif
                           <!--<li><a href="javascript:;"> Configurar mi Perfil</a></li>-->
                           <!--<li>
                              <a href="javascript:;">
                                <span class="badge bg-red pull-right">50%</span>
                                <span>Settings</span>
                              </a>
                              </li>-->
                           <li><a href="javascript:;">Manual de ayuda</a></li>
                           <li>
                              <a href="{{ url('/logout') }}"><i class="fa fa-sign-out pull-right"></i> Cerrar mi sesión</a>
                           </li>
                        </ul>
                     </li>
                     <li role="presentation" class="dropdown">
                        <!--  <a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">
                           <i class="fa fa-envelope-o"></i>
                           <span class="badge bg-green">6</span>
                           </a>-->
                        <!--<ul id="menu1" class="dropdown-menu list-unstyled msg_list" role="menu">
                           <li>
                             <a>
                               <span class="image"><img src="images/img.jpg" alt="Profile Image" /></span>
                               <span>
                                 <span>John Smith</span>
                                 <span class="time">3 mins ago</span>
                               </span>
                               <span class="message">
                                 Film festivals used to be do-or-die moments for movie makers. They were where...
                               </span>
                             </a>
                           </li>
                           <li>
                             <a>
                               <span class="image"><img src="images/img.jpg" alt="Profile Image" /></span>
                               <span>
                                 <span>John Smith</span>
                                 <span class="time">3 mins ago</span>
                               </span>
                               <span class="message">
                                 Film festivals used to be do-or-die moments for movie makers. They were where...
                               </span>
                             </a>
                           </li>
                           <li>
                             <a>
                               <span class="image"><img src="images/img.jpg" alt="Profile Image" /></span>
                               <span>
                                 <span>John Smith</span>
                                 <span class="time">3 mins ago</span>
                               </span>
                               <span class="message">
                                 Film festivals used to be do-or-die moments for movie makers. They were where...
                               </span>
                             </a>
                           </li>
                           <li>
                             <a>
                               <span class="image"><img src="images/img.jpg" alt="Profile Image" /></span>
                               <span>
                                 <span>John Smith</span>
                                 <span class="time">3 mins ago</span>
                               </span>
                               <span class="message">
                                 Film festivals used to be do-or-die moments for movie makers. They were where...
                               </span>
                             </a>
                           </li>
                           <li>
                             <div class="text-center">
                               <a>
                                 <strong>See All Alerts</strong>
                                 <i class="fa fa-angle-right"></i>
                               </a>
                             </div>
                           </li>
                           </ul>
                           </li>
                           </ul>-->
                  </nav>
               </div>
            </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <footer>
          <div class="pull-right">
           PayApp © 2018
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>

    <!-- jQuery -->
    <script src="{{asset('vendors/jquery/dist/jquery.min.js')}}"></script>
    <!-- Bootstrap -->
    <script src="{{asset('vendors/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <!-- FastClick -->
    <script src="{{asset('vendors/fastclick/lib/fastclick.js')}}"></script>
    <!-- NProgress -->
    <script src="{{asset('vendors/nprogress/nprogress.js')}}"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="{{asset('vendors/moment/min/moment.min.js')}}"></script>
    <script src="{{asset('vendors/bootstrap-daterangepicker/daterangepicker.js')}}"></script>
    <!-- bootstrap-datetimepicker -->    
    <script src="{{asset('vendors/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js')}}"></script>
    <!-- Ion.RangeSlider -->
    <script src="{{asset('vendors/ion.rangeSlider/js/ion.rangeSlider.min.js')}}"></script>
    <!-- Bootstrap Colorpicker -->
    <script src="{{asset('vendors/mjolnic-bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js')}}"></script>
    <!-- jquery.inputmask -->
    <script src="{{asset('vendors/jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js')}}"></script>
    <!-- jQuery Knob -->
    <script src="{{asset('vendors/jquery-knob/dist/jquery.knob.min.js')}}"></script>
    <!-- Cropper -->
    <script src="{{asset('vendors/cropper/dist/cropper.min.js')}}"></script>

    <!-- Custom Theme Scripts -->
    <script src="{{asset('js/custom.min.js')}}"></script>
    
    <!-- Initialize datetimepicker -->
<script>
    $('#myDatepicker').datetimepicker();
    
    $('#myDatepicker2').datetimepicker({
        format: 'DD.MM.YYYY'
    });
    
    $('#myDatepicker3').datetimepicker({
        format: 'hh:mm A'
    });
    
    $('#myDatepicker4').datetimepicker({
        ignoreReadonly: true,
        allowInputToggle: true
    });

    $('#datetimepicker6').datetimepicker();
    
    $('#datetimepicker7').datetimepicker({
        useCurrent: false
    });
    
    $("#datetimepicker6").on("dp.change", function(e) {
        $('#datetimepicker7').data("DateTimePicker").minDate(e.date);
    });
    
    $("#datetimepicker7").on("dp.change", function(e) {
        $('#datetimepicker6').data("DateTimePicker").maxDate(e.date);
    });
</script>
  </body>
</html>
