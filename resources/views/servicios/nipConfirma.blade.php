@extends('layouts.logged')
@section('content')

<div class="col-md-12 col-sm-12 col-xs-12">
   <div class="page-title">
      <div class="title_left">
         <h3>Recargas Electronicas</h3>
      </div>
   </div>
   <form role="form" method="POST" action="{{ url('/add-recargar') }}">
          @if( $message == 1 )
                    <div class="alert alert-danger">
                       <h1>NIP Incorrecto. </h1>
                    </div>
          @endif
          
          {{ csrf_field() }}

          @if(session()->has('field_errors'))
                <div class="col-md-12 col-xs-12 w3-panel w3-red w3-display-container">
                  <span onclick="this.parentElement.style.display='none'" class="w3-button w3-red w3-large w3-display-topright">×</span>
                  <h3>Datos Requeridos!</h3>
                    @foreach (session()->get('field_errors')->all() as $error)
                    <div>{{ $error }}</div>
                  @endforeach
                </div>
          @endif

   <div class="col-md-6 col-xs-12">
      <div class="x_panel">
         <div class="x_title">
            <h2>NIP de Seguridad</h2>
         </div>
         <div class="x_content">
            <br>

            <div class="form-group">
              <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                <label>Compañia</label>
                  <input type="text" class="form-control" value="{{$readonlys['compania']}}" id="textCompania" readonly placeholder="telcel">
              </div>
              <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                <label>Monto</label>
                  <input type="text" class="form-control" value="${{$readonlys['monto']}}" id="textMonto" readonly placeholder="500.00">
              </div>
              <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                <label>Número Destinatario</label>
                  <input type="text" class="form-control" id="textnumero" value="{{$readonlys['telefono']}}" readonly placeholder="500.00">
              </div>
               <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                 <input type="text" class="form-control" id="textNip" name="textNip" required placeholder="00000000" style="height: 65px;font-size: 24px;" />
                 <input type="hidden" name="data" value="{{$data}}">
                 <input type="hidden" name="lectura" value="{{$readonlyss}}">
                 <br/>
                 <a href="{{ url('/recargas') }}" class="btn btn-success btn-lg pull-left">Cancelar</a>
                 <button class="btn btn-success btn-lg pull-right" type="submit">Recarga</button>
               </div>
            </div>
         </div>
      </div>
   </div>
   
    </form>
             
</div>

<style type="text/css">
  label > input{ /* HIDE RADIO */
  display:none;
}
label > input + img{ /* IMAGE STYLES */
  cursor:pointer;
  border:2px solid transparent;
  -webkit-filter: brightness(1) grayscale(2) opacity(0.5);
      
            width: 100%;
}
label > input:checked + img{ /* (CHECKED) IMAGE STYLES */
  border:2px solid #dcd4d4;
   -webkit-filter: none;
       -moz-filter: none;
            filter: none;
            -webkit-box-shadow: 2px 2px 5px #999;
  -moz-box-shadow: 2px 2px 5px #999;
}
label > input:checked + img:hover{
   /* -webkit-filter: brightness(1) grayscale(3) opacity(0.5);*/
      
}
.btn span.glyphicon {         
  opacity: 0;       
}
.btn.active span.glyphicon {        
  opacity: 1;       
}

input:required,
textarea:required {
  border-color: red !important;
}
h5{
  font-size: 18px;
}
.error
{
color:red;
font-family:verdana, Helvetica;
}
#monto, #phones {
  display: none;
}

ul.chec-radio {
    
}
ul.chec-radio li.pz {
    /*display: inline;*/
    list-style-type: none;
}
.chec-radio label.radio-inline input[type="checkbox"] {
    display: none;
}
.chec-radio label.radio-inline input[type="checkbox"]:checked+div {
    color: #fff;
    background-color: #000;
}
.chec-radio .radio-inline .clab {
    cursor: pointer;
    background: #5bc0de;
    padding: 12px 30px;
    text-align: center;
    text-transform: uppercase;
    color: #333;
    width: 200px;
    position: relative;
    height: 45px;
    float: left;
    margin: 0;
    margin-bottom: 5px;
    border-color: #46b8da;
}
.chec-radio label.radio-inline input[type="checkbox"]:checked+div:before {
    content: "\e013";
    margin-right: 5px;
    font-family: 'Glyphicons Halflings';
}
.chec-radio label.radio-inline input[type="radio"] {
    display: none;
}
.chec-radio label.radio-inline input[type="radio"]:checked+div {
    color: #fff;
    background-color: #000;
}
.chec-radio label.radio-inline input[type="radio"]:checked+div:before {
    content: "\e013";
    margin-right: 5px;
    font-family: 'Glyphicons Halflings';
}

.w3-red, .w3-hover-red:hover {
    color: #fff!important;
    background-color: #f44336!important;
}
.w3-panel {
    margin-top: 16px;
    margin-bottom: 16px;
}
.w3-container, .w3-panel {
    padding: 0.01em 16px;
}
.w3-tooltip, .w3-display-container {
    position: relative;
}
</style>

<script type="text/javascript" src="//code.jquery.com/jquery-1.5.2.js"></script>
<script type="text/javascript">
              var input =  document.getElementById('textNip');
                              input.addEventListener('input',function(){
                                if (this.value.length > 8) 
                                   this.value = this.value.slice(0,8); 
                              })

                           
</script>
@endsection
