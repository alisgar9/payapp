@extends('layouts.logged')
@section('content')

<div class="col-md-12 col-sm-12 col-xs-12">
   <div class="page-title">
      <div class="title_left">
         <h3>Pagar Servicios</h3>
      </div>
   </div>
   <form role="form" method="POST" action="{{ url('/recargas-add') }}">
          {{ csrf_field() }}

          @if(session()->has('field_errors'))
                <div class="col-md-12 col-xs-12 w3-panel w3-red w3-display-container">
                  <span onclick="this.parentElement.style.display='none'" class="w3-button w3-red w3-large w3-display-topright">×</span>
                  <h3>Datos Requeridos!</h3>
                    @foreach (session()->get('field_errors')->all() as $error)
                    <div>{{ $error }}</div>
                  @endforeach
                </div>
              @endif

   <div class="col-md-8 col-xs-12">
      <div class="x_panel">
         <div class="x_title">
            <h2>Selecciona el Servicio</h2>
         </div>
         <div class="x_content">
            <br>
            <div class="form-group">
               <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                 @foreach ($companias as $com)
                   <div class="col-md-3 col-xs-12 text-center">
                       <label>
                       <input type="radio" id="compania" name="compania" value="{{$com->id}}" class="sr-only" required />
                       <img src="<?php echo url($com->imagen); ?>">
                       </label>
                    </div>
                 @endforeach
                  
                  
               </div>
            </div>
         </div>
      </div>
   </div>
      <div class="col-md-4 col-xs-12" id="phones">
        <div class="x_panel">
         <div class="x_title text-center">
            <img id='logoserv' width="30%" >
         </div>
         <div class="x_content">
          <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
            <label>Referencia</label>
              <input type="text" class="form-control" id="textReferencia1" name="textReferencia1" />
          </div>
          <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
              <input type="text" class="form-control" id="textReferencia2" name="textReferencia2" placeholder="Confirmar Numero de Referencia" />
          </div>
          <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
            <label>Importe Total a Pagar</label>
              <input type="number" class="form-control" id="textMonto" name="textMonto" placeholder="500.00">
          </div>
          <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
            <label>Total a cobrar</label>
              <input type="text" class="form-control" id="textTotal" placeholder="00.00" readonly>
          </div>

          <button class="btn btn-success btn-lg pull-right" type="submit" data-userway-font-size="18">Realizar Cobro</button>
        </div>  
      </div>  
    </form>
             
</div>

<style type="text/css">
  label > input{ /* HIDE RADIO */
  display:none;
}
label > input + img{ /* IMAGE STYLES */
  cursor:pointer;
  border:2px solid transparent;
  -webkit-filter: brightness(1) grayscale(2) opacity(0.5);
      
            width: 100%;
}
label > input:checked + img{ /* (CHECKED) IMAGE STYLES */
  border:2px solid #dcd4d4;
   -webkit-filter: none;
       -moz-filter: none;
            filter: none;
            -webkit-box-shadow: 2px 2px 5px #999;
  -moz-box-shadow: 2px 2px 5px #999;
}
label > input:checked + img:hover{
   /* -webkit-filter: brightness(1) grayscale(3) opacity(0.5);*/
      
}
.btn span.glyphicon {         
  opacity: 0;       
}
.btn.active span.glyphicon {        
  opacity: 1;       
}

input:required,
textarea:required {
  border-color: red !important;
}
h5{
  font-size: 18px;
}
.error
{
color:red;
font-family:verdana, Helvetica;
}
#monto, #phones {
  display: none;
}

ul.chec-radio {
    
}
ul.chec-radio li.pz {
    /*display: inline;*/
    list-style-type: none;
}
.chec-radio label.radio-inline input[type="checkbox"] {
    display: none;
}
.chec-radio label.radio-inline input[type="checkbox"]:checked+div {
    color: #fff;
    background-color: #000;
}
.chec-radio .radio-inline .clab {
    cursor: pointer;
    background: #5bc0de;
    padding: 12px 30px;
    text-align: center;
    text-transform: uppercase;
    color: #333;
    width: 200px;
    position: relative;
    height: 45px;
    float: left;
    margin: 0;
    margin-bottom: 5px;
    border-color: #46b8da;
}
.chec-radio label.radio-inline input[type="checkbox"]:checked+div:before {
    content: "\e013";
    margin-right: 5px;
    font-family: 'Glyphicons Halflings';
}
.chec-radio label.radio-inline input[type="radio"] {
    display: none;
}
.chec-radio label.radio-inline input[type="radio"]:checked+div {
    color: #fff;
    background-color: #000;
}
.chec-radio label.radio-inline input[type="radio"]:checked+div:before {
    content: "\e013";
    margin-right: 5px;
    font-family: 'Glyphicons Halflings';
}

.w3-red, .w3-hover-red:hover {
    color: #fff!important;
    background-color: #f44336!important;
}
.w3-panel {
    margin-top: 16px;
    margin-bottom: 16px;
}
.w3-container, .w3-panel {
    padding: 0.01em 16px;
}
.w3-tooltip, .w3-display-container {
    position: relative;
}
</style>

<script type="text/javascript" src="//code.jquery.com/jquery-1.5.2.js"></script>
<script type="text/javascript">

  $(document).ready(function() {

    $("input:radio[name$='compania']").click(function() {
        var servicio = $(this).val();
        var url = "{{ url('/servicio-view/') }}";
          
        $.ajax({  
          url:url,  
          method:"POST",  
          data:{
            "_token": "{{ csrf_token() }}",
            "servicio":servicio},  
          success:function(data){ 
            if (data.length === 0) {
                $('#error_message').html("Error");
            }else{
                $("#phones").show();
                $("input:radio[name$='compania']").click(function() {
                  $('#textReferencia1').hidden();
                  $('#textReferencia2').hidden();
                });
                var src1 = data.imagen;
                             $("#logoserv").attr("src", src1);

                var referencia1, referencia2;

                              referencia1 = document.getElementById('textReferencia1');
                              referencia2 = document.getElementById('textReferencia2');

                              var tamano;
                              if(data.idservice == 1){
                                tamano = 12;
                              }
                              else if(data.idservice == 2){
                                tamano = 10;
                              }
                              else if(data.idservice == 3){
                                tamano = 12;
                              }else{
                                tamano = 10;
                              }

                              //alert(tamano);

                              referencia1.addEventListener('input',function(){
                                if (this.value.length > tamano) 
                                   this.value = this.value.slice(0,tamano); 
                              })
                              
                              //alert(tamano);

                              referencia1.onchange = referencia2.onkeyup = passwordMatch; 

                              function passwordMatch() {
                                  if(referencia1.value !== referencia2.value)
                                      referencia2.setCustomValidity('Los Numeros no Coinciden.');
                                  else
                                      referencia2.setCustomValidity('');
                              }

            }    
                      
          }  
        });

        //$("#textCompania").val(nombre);
    });

    
   

    


});
  
</script>
@endsection
