<link href="{{asset('vendors/bootstrap/dist/css/bootstrap.min.css')}}" rel="stylesheet">
      <link href="{{asset('vendors/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">
      <link href="{{asset('vendors/nprogress/nprogress.css')}}" rel="stylesheet">
      <link href="{{asset('vendors/iCheck/skins/flat/green.css')}}" rel="stylesheet">
      <link href="{{asset('vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css')}}" rel="stylesheet">
      <link href="{{asset('vendors/jqvmap/dist/jqvmap.min.css')}}" rel="stylesheet"/>
      <link href="{{asset('vendors/bootstrap-daterangepicker/daterangepicker.css')}}" rel="stylesheet">
      <link rel="stylesheet" type="text/css" href="{{asset('js/datatables/datatables.min.css')}}"/>
      <link rel="stylesheet" href="{{asset('vendors/easyautocomplete/easy-autocomplete.css')}}">
      <link rel=“stylesheet” href="{{asset('vendors/easyautocomplete/easy-autocomplete.themes.min.css')}}">
      <link href="https://cdn.rawgit.com/harvesthq/chosen/gh-pages/chosen.min.css" rel="stylesheet"/>
      <link href="{{asset('vendors/pnotify/dist/pnotify.css')}}" rel="stylesheet">
      <link href="{{asset('vendors/pnotify/dist/pnotify.buttons.css')}}" rel="stylesheet">
      <link href="{{asset('vendors/pnotify/dist/pnotify.nonblock.css')}}" rel="stylesheet">
      <link href="{{asset('build/css/custom.min.css')}}" rel="stylesheet">
<br/><br/><br/><br/><br/><br/><br/><br/>
<hr class="estilo">
<div class="text-center col-md-12" style="background-color: #f4f4f4;color:#000000;" />
  <div class="text-center col-md-6">
    <br><br><br>
    <img src="images/smserror.png"><br /><br /><br />
  </div>
  <div class="text-center col-md-6">
    <br/><br/><br/><br/><br/><br/>
    <h1>Hubo un error al tratar de enviar el token via SMS.</h1>
    <h1>Intentelo más tarde!!</h1><br /><br />
    <a href="{{ url('/logout')}}"><button name="button" class="boton-stylo">Regresar</button></a>
  </div>  
</div>  


<style media="screen">

  .margen{
    width: 200px;
    height: 25px;
    margin: 15px;}

  div > input:focus{
    background: #0cac00;
    border: 0;}

  .boton-stylo{
    width: 200px;
    font-size: 17px;
    height: 40px;
    cursor: pointer;
    background-color: #0cac00;
    border: 0;
    color: #fff;}

  .boton-stylo:hover{
    background: black;
    box-shadow: 2px 3px 4px 1px #bfbfbf;}


      body{
        padding: 0px 65px;
      }
      .mayus{
        text-transform: uppercase;}
      .titulo{
        display: flex;
        justify-content: space-around;}
      .estilo{
        border:#0cac00 5px solid;}
      .titulos{
        color:#0cac00;}
      .texto{
        display: flex;}
      .estilo-inputo-dos:hover{
        border: #0cac00 1px solid;
      }
    </style>
