<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>PayApp ©  2018</title>

    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <!-- Font Awesome -->
    <link href="{{asset('vendors/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">
    <!-- NProgress -->
    <link href="{{asset('vendors/nprogress/nprogress.css')}}" rel="stylesheet">
    <!-- Animate.css -->
    <link href="{{asset('vendors/animate.css/animate.min.css')}}" rel="stylesheet">
    <!-- PNotify -->
    <link href="{{asset('vendors/pnotify/dist/pnotify.css')}}" rel="stylesheet">
    <link href="{{asset('vendors/pnotify/dist/pnotify.buttons.css')}}" rel="stylesheet">
    <link href="{{asset('vendors/pnotify/dist/pnotify.nonblock.css')}}" rel="stylesheet">
    <link href="{{asset('build/css/custom.min.css')}}" rel="stylesheet">

    <script type="text/javascript">
    var _userway_config = {
    // uncomment the following line to override default position
    position: 5,
    // uncomment the following line to override default language (e.g., fr, de, es, he, nl, etc.)
     language: 'es',
    // uncomment the following line to override color set via widget
     //color: '#eed81e',
    account: 'WPGtFQ2cby'
    };
    </script>
    <script type="text/javascript" src="https://cdn.userway.org/widget.js"></script>
  </head>

<body class="login">
<style>
  body{
    background: transparent;
    overflow-x: hidden;
  }
  .clearing{
    clear: both !important;
  }
  .center-align{
    text-align: center !important;
  }
  .right-align{
    text-align: right;
  }
  .content-logo{
    background: #fff;
    border:0;
  }
  .logo img{
    width: 40%;
  }
  .main-login{
    margin-top:-1px;
  }
  .icon-user img{
    width: 15%;
  }
  .card-form{
    margin-top: 30px;
    background: rgba(255,255,255,.8);
    padding: 50px 20px 20px;
    border-radius: 10px;
    position: relative;
    box-shadow: 0 10px 16px 0 rgba(0,0,0,0.2),0 6px 20px 0 rgba(0,0,0,0.19) !important;
  }
  .title-sistema{
    font-size: 28px;
  }
  .title-form{
    position: absolute;
    background: rgb(12, 172, 0);
    color: #fff;
    top: 0px;
    left: 50%;
    transform: translate(-50%, -50%);
    -webkit-transform: translate(-50%, -50%);
    padding: 15px 40px;
    width: 80%;
    text-align: center;
    font-size: 25px;
  }
  .information-login{
    margin-top: 10px;
  }

  .acceso-proveedores{
    padding-top: 25px;
    padding-right: 25px;
  }
  .acceso-proveedores a{
    font-size: 23px;
    color: #4F350D;
    text-shadow: 1px 1px #8c6400;
  }
</style>

  <div class="row">
    <a class="hiddenanchor" id="signup"></a>
    <a class="hiddenanchor" id="signin"></a>
    <!-- Logo -->
    <div class="col-md-12 content-logo">
      <div class="col-md-4 logo">
        <br>
      </div>
    </div>
  </div>
  <div class="row">

    <!-- Formulario de Sesión -->
    <div class="container main-login">
       <div class="form login_form">
                @if(session()->has('message'))
                    <div class="alert alert-danger">
                        {{ session()->get('message') }}
                    </div>
                @endif
        </div>
      <div class="center-align icon-user" style="padding-top: 115px; padding-bottom: 50px;">
        <img src="{{asset('images/logopayapp.png')}}" alt="User-login">
      </div>  
      <div class="center-align icon-user">
        <h1 class="title-sistema">Sistema de Prepago PayApp</h1>
      </div>
      <div class="card-form col-md-6 col-md-offset-3">
        <div class="title-form">
         Cambiar Contraseña
        </div>

<div class="col-md-12 col-xs-12">
 <div class="x_panel">
                  
                
                  <div class="x_content">
                    <br>
                    <form class="form-horizontal form-label-left"  method="POST" action="{{ url('/update-contrasena-f') }}">
                      {{ csrf_field() }}
                      @if(session()->has('message'))
                          <div class="alert alert-success">
                              {{ session()->get('message') }}
                          </div>
                      @endif
                      @if(session()->has('field_errors'))
                            <div class="col-md-12 col-xs-12 w3-panel w3-red w3-display-container">
                              <span onclick="this.parentElement.style.display='none'" class="w3-button w3-red w3-large w3-display-topright">×</span>
                              <h3>Datos Requeridos!</h3>
                                @foreach (session()->get('field_errors')->all() as $error)
                                <div>{{ $error }}</div>
                              @endforeach
                            </div>
                      @endif
                      <div class="form-group">
                        <h4>Tu contraseña debe contener 8 caracteres</h4>
                      </div>  
                      <div class="form-group">
                        <label class="control-label col-md-4 col-sm-3 col-xs-12">Contrasena Nueva</label>
                        <div class="col-md-8 col-sm-9 col-xs-12">
                          <input type="password" name="pass1" id="pass1" class="form-control" width="100%" required />
                          
                        </div>
                      </div>
                       <div class="form-group">
                        <label class="control-label col-md-4 col-sm-3 col-xs-12">Confirmar Contraseña Nueva</label>
                        <div class="col-md-8 col-sm-9 col-xs-12">
                          <input type="password" name="pass2" id="pass2" class="form-control" required width="100%" />
                        </div>
                      </div>
                      <input type="hidden" name="tokend" value="{{$cve_usuario1}}" >
                      
                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-sm-12">
                        
                        <div class="col-md-6 text-center">
                          <button type="submit" class="btn btn-success"><h2>Actualizar</h2></button>
                        </div>
                        <div class="col-md-6 text-left">
                          <a href="{{ url('/login') }}" class="btn btn-success"><h2>Cancelar</h2></a>
                        </div>
                      </div>
                      </div>
                      

                    </form>
                  </div>
                </div>
              </div>
<div class="clearing"></div>
        
      </div>
    </div> <!-- Termina Formulario -->
  </div> <!-- Cierra Row -->
  <div class="clearing"></div>
</body>
</html>
<!-- jQuery -->
<script src="{{asset('vendors/jquery/dist/jquery.min.js')}}"></script>
<!-- PNotify -->
<script src="{{asset('vendors/pnotify/dist/pnotify.js')}}"></script>
<script src="{{asset('vendors/pnotify/dist/pnotify.buttons.js')}}"></script>
<script src="{{asset('vendors/pnotify/dist/pnotify.nonblock.js')}}"></script>

<script>
// Get IE or Edge browser version
var version = detectIE();

if (version === false) {


} else if (version >= 12) {
  new PNotify({
    title: 'Actualice su Navegador',
    text: 'El sistema requiere un navegador con soporte HTML5 para poder usar este sistema es necesario que actualice el suyo o bien utilice alguno de los que proponemos a continuación: Google Chrome o Mozilla Firefox.',
    hide: false,
    styling: 'bootstrap3'
  });
} else {
  new PNotify({
    title: 'Actualice su Navegador',
    text: 'El sistema requiere un navegador con soporte HTML5 para poder usar este sistema es necesario que actualice el suyo o bien utilice alguno de los que proponemos a continuación: Google Chrome o Mozilla Firefox.',
    hide: false,
    styling: 'bootstrap3'
  });
}

function detectIE() {
  var ua = window.navigator.userAgent;

  // Test values; Uncomment to check result …

  // IE 10
  // ua = 'Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.2; Trident/6.0)';
  
  // IE 11
  // ua = 'Mozilla/5.0 (Windows NT 6.3; Trident/7.0; rv:11.0) like Gecko';
  
  // Edge 12 (Spartan)
  // ua = 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.71 Safari/537.36 Edge/12.0';
  
  // Edge 13
  // ua = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2486.0 Safari/537.36 Edge/13.10586';

  var msie = ua.indexOf('MSIE ');
  if (msie > 0) {
    // IE 10 or older => return version number
    return parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
  }

  var trident = ua.indexOf('Trident/');
  if (trident > 0) {
    // IE 11 => return version number
    var rv = ua.indexOf('rv:');
    return parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
  }

  var edge = ua.indexOf('Edge/');
  if (edge > 0) {
    // Edge (IE 12+) => return version number
    return parseInt(ua.substring(edge + 5, ua.indexOf('.', edge)), 10);
  }

  // other browser
  return false;
}
</script> 


  <style>
  .w3-red, .w3-hover-red:hover {
      color: #fff!important;
      background-color: #f44336!important;
  }
  .w3-panel {
      margin-top: 16px;
      margin-bottom: 16px;
  }
  .w3-container, .w3-panel {
      padding: 0.01em 16px;
  }
  .w3-tooltip, .w3-display-container {
      position: relative;
  }
  </style>
<script type="text/javascript" src="//code.jquery.com/jquery-1.5.2.js"></script>
<script type="text/javascript">

  $(document).ready(function() {
    var pass1, pass2;

                              pass1 = document.getElementById('pass1');
                              pass2 = document.getElementById('pass2');

                              pass1.onchange = pass2.onkeyup = passwordMatch; 

                              function passwordMatch() {
                                  if(pass1.value !== pass2.value)
                                      pass2.setCustomValidity('Las Contraseñas no Coinciden.');
                                  else
                                      pass2.setCustomValidity('');
                              }

                              var input=  document.getElementById('pass1');
                              input.addEventListener('input',function(){
                                if (this.value.length > 8) 
                                   this.value = this.value.slice(0,8); 
                              })

                              var input2=  document.getElementById('pass2');
                              input2.addEventListener('input',function(){
                                if (this.value.length > 8) 
                                   this.value = this.value.slice(0,8); 
                              })


  });
</script>
