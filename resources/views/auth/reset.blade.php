<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>PayApp ©  2018</title>

    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <!-- Font Awesome -->
    <link href="{{asset('vendors/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">
    <!-- NProgress -->
    <link href="{{asset('vendors/nprogress/nprogress.css')}}" rel="stylesheet">
    <!-- Animate.css -->
    <link href="{{asset('vendors/animate.css/animate.min.css')}}" rel="stylesheet">
    <!-- PNotify -->
    <link href="{{asset('vendors/pnotify/dist/pnotify.css')}}" rel="stylesheet">
    <link href="{{asset('vendors/pnotify/dist/pnotify.buttons.css')}}" rel="stylesheet">
    <link href="{{asset('vendors/pnotify/dist/pnotify.nonblock.css')}}" rel="stylesheet">
    <link href="{{asset('build/css/custom.min.css')}}" rel="stylesheet">

    <script type="text/javascript">
    var _userway_config = {
    // uncomment the following line to override default position
    position: 5,
    // uncomment the following line to override default language (e.g., fr, de, es, he, nl, etc.)
     language: 'es',
    // uncomment the following line to override color set via widget
     //color: '#eed81e',
    account: 'WPGtFQ2cby'
    };
    </script>
    <script type="text/javascript" src="https://cdn.userway.org/widget.js"></script>
  </head>

<body class="login">
<style>
  body{
    background: transparent;
    overflow-x: hidden;
  }
  .clearing{
    clear: both !important;
  }
  .center-align{
    text-align: center !important;
  }
  .right-align{
    text-align: right;
  }
  .content-logo{
    background: #fff;
    border:0;
  }
  .logo img{
    width: 40%;
  }
  .main-login{
    margin-top:-1px;
  }
  .icon-user img{
    width: 15%;
  }
  .card-form{
    margin-top: 30px;
    background: rgba(255,255,255,.8);
    padding: 50px 20px 20px;
    border-radius: 10px;
    position: relative;
    box-shadow: 0 10px 16px 0 rgba(0,0,0,0.2),0 6px 20px 0 rgba(0,0,0,0.19) !important;
  }
  .title-sistema{
    font-size: 28px;
  }
  .title-form{
    position: absolute;
    background: rgb(12, 172, 0);
    color: #fff;
    top: 0px;
    left: 50%;
    transform: translate(-50%, -50%);
    -webkit-transform: translate(-50%, -50%);
    padding: 15px 40px;
    width: 60%;
    text-align: center;
    font-size: 25px;
  }
  .information-login{
    margin-top: 10px;
  }

  .acceso-proveedores{
    padding-top: 25px;
    padding-right: 25px;
  }
  .acceso-proveedores a{
    font-size: 23px;
    color: #4F350D;
    text-shadow: 1px 1px #8c6400;
  }
</style>

  <div class="row">
    <a class="hiddenanchor" id="signup"></a>
    <a class="hiddenanchor" id="signin"></a>
    <!-- Logo -->
    <div class="col-md-12 content-logo">
      <div class="col-md-4 logo">
        <br>
      </div>
    </div>
  </div>
  <div class="row">

    <!-- Formulario de Sesión -->
    <div class="container main-login">
       <div class="form login_form">
                @if(session()->has('message'))
                    <div class="alert alert-danger">
                        {{ session()->get('message') }}
                    </div>
                @endif
        </div>
      <div class="center-align icon-user" style="padding-top: 115px; padding-bottom: 50px;">
        <img src="images/logopayapp.png" alt="User-login">
      </div>  
      <div class="center-align icon-user">
        <h1 class="title-sistema">Sistema de Prepago PayApp</h1>
      </div>
      <div class="card-form col-md-6 col-md-offset-3">
        <div class="title-form">
         Restablecer Contraseña
        </div>

        <form role="form" method="POST" class="col-md-10 col-md-offset-1" action="{{ url('/peticion-restart') }}">
          {{ csrf_field() }}

            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
              <label for="[object Object]">Igresa tu Número Celular</label>
              <input type="text" pattern="[0-9]{10}" class="form-control" autocomplete="off" title="El Número Destinatario debe contener 10 digitos." placeholder="1234567890" id="phone" name="phone" required onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')" />
             
            </div>
            <div>
            <div class="footer text-center">
              <button id="login" type="submit" class="btn btn-fill btn-success btn-wd"><i class="fa fa-sign-in" aria-hidden="true"></i> Enviar Token</button>
              <a href="{{ url('/login') }}"><button type="button"  class="btn btn-fill btn-danger btn-wd"><i class="fa fa-reply" aria-hidden="true"></i> Regresar</button></a>
            </div>
            </div>
            <div class="clearfix"></div>

            <div class="separator">
              <div class="clearfix"></div>
              <br />
              <div class="center-align">
                <p>PayApp la nueva manera de hacer crecer tu negocio. </p>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </body>
</html>
<script type="text/javascript" src="//code.jquery.com/jquery-1.5.2.js"></script>
<script type="text/javascript">

  $(document).ready(function() {

    input.oninvalid = function(event) {
        event.target.setCustomValidity('El Número Destinatario debe contener 10 digitos.');
    }

  });  
  
</script>

