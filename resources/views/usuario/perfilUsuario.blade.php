@extends('layouts.logged')
@section('content')

<div class="col-md-12 col-xs-12">
 <div class="x_panel">
                  <div class="x_title">
                    <h2>Datos de Usuario</h2>
                    <div class="clearfix"></div>
                  </div>
  <br/>
                
                  <div class="x_content">
                    <br>
                    <form class="form-horizontal form-label-left"  method="POST" action="{{ url('/update-user') }}">
                      {{ csrf_field() }}

                      @if(session()->has('message'))
                          <div class="alert alert-success">
                              {{ session()->get('message') }}
                          </div>
                      @endif
                      @if(session()->has('error'))
                          <div class="alert alert-danger">
                              {{ session()->get('error') }}
                          </div>
                      @endif

                      @if(session()->has('field_errors'))
                            <div class="col-md-12 col-xs-12 w3-panel w3-red w3-display-container">
                              <span onclick="this.parentElement.style.display='none'" class="w3-button w3-red w3-large w3-display-topright">×</span>
                              <h3>Datos Requeridos!</h3>
                                @foreach (session()->get('field_errors')->all() as $error)
                                <div>{{ $error }}</div>
                              @endforeach
                            </div>
                      @endif
                      <div class="form-group">
                        <div class="col-md-12">
                            <label class="control-label col-md-12"><h4><b>Tipo de Persona Fiscal</b></h4></label>
                            <div class="radio col-md-12">
                              <div class="col-md-4 col-sm-12 col-xs-12 text-left">
                                <label class="control-label col-md-12">
                                  <input type="radio" value="1" id="optionsRadios2" name="tipop" required 
                                  @if($cliente->tipo_persona == 1) checked @endif /> Persona Fisica 
                                </label>
                              </div>
                              <div class="col-md-4 col-sm-12 col-xs-12 text-left">  
                                <label class="control-label col-md-12">
                                  <input type="radio" value="2" id="optionsRadios2" name="tipop" required 
                                  @if($cliente->tipo_persona == 2) checked @endif /> Persona Moral 
                                </label>
                              </div>  
                              <div class="col-md-4 col-sm-12 col-xs-12 text-left">  
                                <label class="control-label col-md-12">
                                  <input type="radio" value="0" id="optionsRadios2" name="tipop" required 
                                  @if($cliente->tipo_persona == 0) checked @endif /> Ninguna 
                                </label>
                              </div>  
                            </div>
                            
                      </div>
                      <div class="form-group">
                        
                        <div class="col-md-4 col-sm-12 col-xs-12 text-left">
                            <label class="control-label col-md-12">Razon Social</label>
                            <input type="text" name="cliente" id="cliente" required value="{{$cliente->cliente}}" class="form-control">
                        </div>
                        <div class="col-md-4 col-sm-9 col-xs-12 text-left" id="rfcd">
                          
                        </div>
                        <div class="col-md-4 col-sm-9 col-xs-12 text-left">
                             <label class="control-label col-md-12 col-sm-3 col-xs-12">Correo Electronico</label>
                            <input type="email" name="email" id="email" value="{{$cliente->email}}" required class="form-control">
                        </div>
                      </div>
                      <div class="form-group text-center">
                        <h4><b>Nombre del Representante</b></h4>
                      </div>

                      <div class="form-group">
                        <div class="col-md-4 col-sm-9 col-xs-12">
                            <label class="control-label col-md-12 col-sm-3 col-xs-12">Nombre(s)</label>
                            <input type="text" name="nombres" id="nombres" value="{{$cliente->nombres}}" required class="form-control">
                        </div>
                        <div class="col-md-4 col-sm-9 col-xs-12">
                          <label class="control-label col-md-12 col-sm-3 col-xs-12">Apellido Paterno</label>
                            <input type="text" name="apellido1" id="apellido1" value="{{$cliente->apellido1}}" required class="form-control">
                        </div>
                        <div class="col-md-4 col-sm-9 col-xs-12">
                          <label class="control-label col-md-12 col-sm-3 col-xs-12">Apellido Materno</label>
                            <input type="text" name="apellido2" id="apellido2" value="{{$cliente->apellido2}}" required class="form-control">
                        </div>

                      </div>
                      <div class="form-group text-center">
                        <h4><b>Domicilio</b></h4>
                      </div>
                      <div class="form-group">
                        <div class="col-md-4 col-sm-9 col-xs-12">
                            <label class="control-label col-md-12 col-sm-3 col-xs-12">Calle y Numero</label>
                            <input type="text" name="calle_num" id="calle_num" value="{{$cliente->calle_num}}" required class="form-control">
                        </div>
                        <div class="col-md-4 col-sm-9 col-xs-12">
                          <label class="control-label col-md-12 col-sm-3 col-xs-12">Colonia</label>
                            <input type="text" name="colonia" id="colonia" value="{{$cliente->colonia}}" required class="form-control">
                        </div>
                        <div class="col-md-4 col-sm-9 col-xs-12">
                          <label class="control-label col-md-12 col-sm-3 col-xs-12">Alcaldía o Municipio</label>
                            <input type="text" name="del_mun" id="del_mun" value="{{$cliente->del_mun}}" required class="form-control">
                        </div>

                      </div>
                      <div class="form-group">
                        <div class="col-md-4 col-sm-9 col-xs-12">
                            <label class="control-label col-md-12 col-sm-3 col-xs-12">Ciudad</label>
                            <input type="text" name="ciudad" id="ciudad" value="{{$cliente->ciudad}}" required class="form-control">
                        </div>
                        <div class="col-md-4 col-sm-9 col-xs-12">
                          <label class="control-label col-md-12 col-sm-3 col-xs-12">Codigo Postal</label>
                            <input type="text" name="cp" id="cp" value="{{$cliente->cp}}" pattern="[0-9]{5}" required class="form-control">
                        </div>
                      </div>
                      <div class="form-group text-center">
                        <h4><b>Teléfono</b></h4>
                      </div>
                      <div class="form-group">
                        <div class="col-md-4 col-sm-9 col-xs-12">
                            <label class="control-label col-md-12 col-sm-3 col-xs-12">Teléfono Celular </label>
                            <input type="number" readonly value="{{$cliente->telefono1}}" required class="form-control">
                        </div>
                        <div class="col-md-4 col-sm-9 col-xs-12">
                          <label class="control-label col-md-12 col-sm-3 col-xs-12">Teléfono de Contacto (Opcional)</label>
                            <input type="number" name="telefono2" id="telefono2" value="{{$cliente->telefono2}}"  class="form-control">
                        </div>

                      </div>


                      </div>
                      
                      <div class="form-group">
                        <div class="col-md-12 text-center">
                          <input type="hidden" name="tipo" value="{{$tipo}}">
                          @if($tipo == 1)
                            <button type="submit" class="btn btn-success">La informacion es correcta. Siguiente <i class="fa fa-arrow-right"></i></button>
                          @else
                            <button type="submit" class="btn btn-success">Actualizar</button>
                          @endif
                          
                        </div>
                      </div>
                      

                    </form>
                  </div>
                </div>
              </div>

<style>
.w3-red, .w3-hover-red:hover {
    color: #fff!important;
    background-color: #f44336!important;
}
.w3-panel {
    margin-top: 16px;
    margin-bottom: 16px;
}
.w3-container, .w3-panel {
    padding: 0.01em 16px;
}
.w3-tooltip, .w3-display-container {
    position: relative;
}
.control-label{
  text-align: left !important;
}

#resultado {
    color: #333333;
    font-weight: bold;
}
#resultado.ok {
    background-color: green;
}

</style>
<script type="text/javascript" src="//code.jquery.com/jquery-1.5.2.js"></script>
<script type="text/javascript">
  
  $(document).ready(function() {
    
    var tipops = $('input:radio[name=tipop]:checked').val();
     if(tipops == "1" || tipops == "2" )
     {
        $('#rfcd').append('<label class="control-label col-md-12 col-sm-3 col-xs-12">RFC</label><input type="text" name="rfc" id="rfc" value="{{$cliente->rfc}}" required="" oninput="validarInput(this)"  class="form-control" />');
     }
     
    var cp=  document.getElementById('cp');
    cp.addEventListener('cp',function(){
        if (this.value.length > 5) 
       this.value = this.value.slice(0,5); 
    });

    cp.oninvalid = function(event) {
       event.target.setCustomValidity('El Código Postal debe contener 5 Números.');
    }


    $("input[name=tipop]").change(function () { 
        var tipop = $(this).val();
        if(tipop == "1" || tipop == "2"){
          $('#rfcd').empty();
          $('#rfcd').append('<label class="control-label col-md-12 col-sm-3 col-xs-12">RFC</label><input type="text" name="rfc" id="rfc" value="{{$cliente->rfc}}" required="" oninput="validarInput(this)"  class="form-control" /><pre id="resultado"></pre>'); 
        } 
        else{
          $('#rfcd').empty();
        }
      });
    

    var rfc =  document.getElementById('rfc');
    rfc.addEventListener('input',function(){
        if (this.value.length > 13) 
       this.value = this.value.slice(0,13); 
    });
    
    rfc.oninvalid = function(event) {
       event.target.setCustomValidity('El RFC debe contener entre 12 y 13 caracteres.');
    }

  });

  function rfcValido(rfc, aceptarGenerico = true) {
    const re       = /^([A-ZÑ&]{3,4}) ?(?:- ?)?(\d{2}(?:0[1-9]|1[0-2])(?:0[1-9]|[12]\d|3[01])) ?(?:- ?)?([A-Z\d]{2})([A\d])$/;
    var   validado = rfc.match(re);

    if (!validado)  //Coincide con el formato general del regex?
        return false;

    //Separar el dígito verificador del resto del RFC
    const digitoVerificador = validado.pop(),
          rfcSinDigito      = validado.slice(1).join(''),
          len               = rfcSinDigito.length,

    //Obtener el digito esperado
          diccionario       = "0123456789ABCDEFGHIJKLMN&OPQRSTUVWXYZ Ñ",
          indice            = len + 1;
    var   suma,
          digitoEsperado;

    if (len == 12) suma = 0
    else suma = 481; //Ajuste para persona moral

    for(var i=0; i<len; i++)
        suma += diccionario.indexOf(rfcSinDigito.charAt(i)) * (indice - i);
    digitoEsperado = 11 - suma % 11;
    if (digitoEsperado == 11) digitoEsperado = 0;
    else if (digitoEsperado == 10) digitoEsperado = "A";

    //El dígito verificador coincide con el esperado?
    // o es un RFC Genérico (ventas a público general)?
    if ((digitoVerificador != digitoEsperado)
     && (!aceptarGenerico || rfcSinDigito + digitoVerificador != "XAXX010101000"))
        return false;
    else if (!aceptarGenerico && rfcSinDigito + digitoVerificador == "XEXX010101000")
        return false;
    return rfcSinDigito + digitoVerificador;
}


//Handler para el evento cuando cambia el input
// -Lleva la RFC a mayúsculas para validarlo
// -Elimina los espacios que pueda tener antes o después
function validarInput(input) {
    var rfc         = input.value.trim().toUpperCase(),
        resultado   = document.getElementById("resultado"),
        valido;
        
    var rfcCorrecto = rfcValido(rfc);   // ⬅️ Acá se comprueba
  
    if (rfcCorrecto) {
      valido = "Válido";
      resultado.classList.add("ok");
    } else {
      valido = "No válido"
      resultado.classList.remove("ok");

      $( "#rfc" ).focus();

    }
        
    resultado.innerText =  'RFC ' + valido;
}

</script>
 
@endsection
