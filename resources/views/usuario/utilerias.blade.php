@extends('layouts.logged')
@section('content')

<div class="col-md-12 col-xs-12">
   <div class="x_panel">
      <div class="x_title">
         <h2>Cuentas de Banco</h2>
         <div class="clearfix"></div>
      </div>
      <br/>
      <div class="x_content">
        

        
        <div class="col-md-12 form-group">
          <div class="col-md-3 text-center">
            <div class="card card-pricing popular shadow text-center px-3 mb-4">
            <div class="bg-transparent card-header pt-4 border-0 text-center">
                <h1 class="h1 font-weight-normal text-primary text-center mb-0" data-pricing-value="30"><img src="{{asset('images/bancoa.jpg')}}" class="img-responsive  center-block"></h1>
            </div>
            <div class="card-body pt-0">
                <ul class="list-unstyled mb-4" style="font-size:17px">
                    <li><b>Banco Azteca:</b><br> 01720187427599</li>
                    <li><b>CLABE:</b><br> 127180001874275999</li>
                    <li><b>Cliente:</b><br> TU MUNDO DE PREPAGO SA DE CV</li>
                </ul>
                <br/>
            </div>
        </div>
          </div>
          <div class="col-md-3 text-center">
            <div class="card card-pricing popular shadow text-center px-3 mb-4">
            <div class="bg-transparent card-header pt-4 border-0 text-center">
                <h1 class="h1 font-weight-normal text-primary text-center mb-0" data-pricing-value="30"><img src="{{asset('images/bancomer.jpg')}}" class="img-responsive  center-block"></h1>
            </div>
            <div class="card-body pt-0">
                <ul class="list-unstyled mb-4"  style="font-size:17px">
                    <li><b>Bancomer:</b><br> 0198283036</li>
                    <li><b>CLABE:</b><br> 012180001982830360</li>
                    <li><b>Cliente:</b><br> TU MUNDO DE PREPAGO SA DE CV</li>
                </ul>
                <br/>
            </div>
        </div>
          </div>
          <div class="col-md-3 text-center">
            <div class="card card-pricing popular shadow text-center px-3 mb-4">
            <div class="bg-transparent card-header pt-4 border-0 text-center">
                <h1 class="h1 font-weight-normal text-primary text-center mb-0" data-pricing-value="30"><img src="{{asset('images/banorte.jpg')}}" class="img-responsive  center-block"></h1>
            </div>
            <div class="card-body pt-0">
                <ul class="list-unstyled mb-4" style="font-size:17px">
                    <li><b>Banorte:</b><br> 0253241694</li>
                    <li><b>CLABE:</b> <br>072180002532416940</li>
                    <li><b>Cliente:</b> <br>TU MUNDO DE PREPAGO SA DE CV</li>
                </ul>
                <br/>
            </div>
        </div>
          </div>
          <div class="col-md-3 text-center">
            <div class="card card-pricing popular shadow text-center px-3 mb-4">
            <div class="bg-transparent card-header pt-4 border-0 text-center">
                <h1 class="h1 font-weight-normal text-primary text-center mb-0" data-pricing-value="30"><img src="{{asset('images/banamex.jpg')}}" class="img-responsive  center-block"></h1>
            </div>
            <div class="card-body pt-0">
                <ul class="list-unstyled mb-4" style="font-size:17px">
                    <li><b>Banamex:</b><br> 35595 Suc: 4390</li>
                    <li><b>CLABE:</b><br> 002180439000355956</li>
                    <li><b>Cliente:</b><br> TU MUNDO DE PREPAGO SA DE CV</li>
                </ul>
                <br/>
            </div>
        </div>
          </div>
          
        </div>  
        <div class="col-md-12 form-group">
          <div class="col-md-3 text-center">
            <div class="card card-pricing popular shadow text-center px-3 mb-4">
            <div class="bg-transparent card-header pt-4 border-0 text-center">
                <h1 class="h1 font-weight-normal text-primary text-center mb-0" data-pricing-value="30"><img src="{{asset('images/oxxo.jpg')}}" class="img-responsive  center-block"></h1>
            </div>
            <div class="card-body pt-0">
                <ul class="list-unstyled mb-4" style="font-size:17px">
                    <li><b>OXXO Bancomer:</b><br> 4555 1030 0619 7944</li>
                    <li><b>Cliente:</b><br> TU MUNDO DE PREPAGO SA DE CV</li>
                </ul>
                <br/>
            </div>
        </div>
          </div>
        </div>



      </div>
   </div>
</div>

<style>
.card-pricing.popular {
    z-index: 1;
    border: 2px solid #ccc;
}
.card-pricing .list-unstyled li {
    padding: .5rem 0;
    color: #6c757d;
}

.w3-red, .w3-hover-red:hover {
    color: #fff!important;
    background-color: #f44336!important;
}
.w3-panel {
    margin-top: 16px;
    margin-bottom: 16px;
}
.w3-container, .w3-panel {
    padding: 0.01em 16px;
}
.w3-tooltip, .w3-display-container {
    position: relative;
}
.control-label{
  text-align: left !important;
}
</style>
@endsection
