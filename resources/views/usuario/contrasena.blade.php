@extends('layouts.logged')
@section('content')
<div class="col-md-12 col-xs-12">
 <div class="x_panel">
                  <div class="x_title">
                    <h2>Cambiar Contraseña</h2>
                    <div class="clearfix"></div>
                  </div>
  <br/>
                
                  <div class="x_content">
                    <br>
                    <form class="form-horizontal form-label-left"  method="POST" action="{{ url('/update-contrasena') }}">
                      {{ csrf_field() }}
                      @if(session()->has('message'))
                          <div class="alert alert-success">
                              {{ session()->get('message') }}
                          </div>
                      @endif
                      @if(session()->has('field_errors'))
                            <div class="col-md-12 col-xs-12 w3-panel w3-red w3-display-container">
                              <span onclick="this.parentElement.style.display='none'" class="w3-button w3-red w3-large w3-display-topright">×</span>
                              <h3>Datos Requeridos!</h3>
                                @foreach (session()->get('field_errors')->all() as $error)
                                <div>{{ $error }}</div>
                              @endforeach
                            </div>
                      @endif
                      <div class="form-group">
                        <h4>Tu contraseña debe contener 8 caracteres</h4>
                      </div>  
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Contrasena Nueva</label>
                        <div class="col-md-4 col-sm-9 col-xs-12">
                          <input type="password" name="pass1" id="pass1" class="form-control" required>
                          
                        </div>
                      </div>
                       <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Confirmar Contraseña Nueva</label>
                        <div class="col-md-4 col-sm-9 col-xs-12">
                          <input type="password" name="pass2" id="pass2" class="form-control" required>
                        </div>
                      </div>
                      
                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-sm-12">
                        
                        <div class="col-md-6 text-center">
                          <button type="submit" class="btn btn-success"><h2>Actualizar</h2></button>
                        </div>
                        <div class="col-md-6 text-left">
                          <a href="{{ url('/home') }}" class="btn btn-success"><h2>Cancelar</h2></a>
                        </div>
                      </div>
                      </div>
                      

                    </form>
                  </div>
                </div>
              </div>

  <style>
  .w3-red, .w3-hover-red:hover {
      color: #fff!important;
      background-color: #f44336!important;
  }
  .w3-panel {
      margin-top: 16px;
      margin-bottom: 16px;
  }
  .w3-container, .w3-panel {
      padding: 0.01em 16px;
  }
  .w3-tooltip, .w3-display-container {
      position: relative;
  }
  </style>
<script type="text/javascript" src="//code.jquery.com/jquery-1.5.2.js"></script>
<script type="text/javascript">

  $(document).ready(function() {
    var pass1, pass2;

                              pass1 = document.getElementById('pass1');
                              pass2 = document.getElementById('pass2');

                              pass1.onchange = pass2.onkeyup = passwordMatch; 

                              function passwordMatch() {
                                  if(pass1.value !== pass2.value)
                                      pass2.setCustomValidity('Las Contraseñas no Coinciden.');
                                  else
                                      pass2.setCustomValidity('');
                              }

                              var input=  document.getElementById('pass1');
                              input.addEventListener('input',function(){
                                if (this.value.length > 8) 
                                   this.value = this.value.slice(0,8); 
                              })

                              var input2=  document.getElementById('pass2');
                              input2.addEventListener('input',function(){
                                if (this.value.length > 8) 
                                   this.value = this.value.slice(0,8); 
                              })


  });
</script>
@endsection
